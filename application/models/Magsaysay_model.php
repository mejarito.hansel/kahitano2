<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");

class Magsaysay_model extends CI_Model {


    public function fetchallApplicantInfo(){
      $sql = $this->db->get('applicant'); //could be any table
         if($sql){
            if($sql->num_rows()>0) {
                $data = $sql->result();
                $response = array('status' => 'SUCCESS',
                                   'message' => 'SUCCESS FETCHING DATA',
                                   'payload' => $data);
               
                echo json_encode($response);
             }
            else{
                $response = array('status' => 'FAILED',
                                   'message' => 'FAILED RETRIEVING DATA');
                 echo json_encode($response);
            }        
         }
         else{
            $response = array('status' => 'ERROR',
                              'message' => 'ERROR');
            echo json_encode($response);            
            $error = $this->db->error();
            print_r($error);
         }
    }

    public function fetchallAdditionalApplicantInfo(){
      $sql = $this->db->get('applicant_additional_info'); //could be any table
         if($sql){
            if($sql->num_rows()>0) {
                $data = $sql->result();
                $response = array('status' => 'SUCCESS',
                                   'message' => 'SUCCESS FETCHING DATA',
                                   'payload' => $data);
               
                echo json_encode($response);
             }
            else{
                $response = array('status' => 'FAILED',
                                   'message' => 'FAILED RETRIEVING DATA');
                 echo json_encode($response);
            }        
         }
         else{
            $response = array('status' => 'ERROR',
                              'message' => 'ERROR');
            echo json_encode($response);            
            $error = $this->db->error();
            print_r($error);
         }
    }
    
    public function submitApplicantInfo($payload){
       if($payload == null){
           $response = array('status' => 'FAILED',
                             'message' => 'PLEASE CHECK YOUR DATA');
           echo json_encode($response);
       }

       elseif($payload != null){
            
            $data = array(
                'applicant_fname' => $payload['applicant_fname'],
                'applicant_mname' => $payload['applicant_mname'], 
                'applicant_lname' => $payload['applicant_lname'], 
                'student_type_ID' => $payload['student_type_ID'], 
                'applicant_email' => $payload['applicant_email'],
                'applicant_password' => base64_encode('magsaysay'),
                'applicant_address' => $payload['applicant_address'],
                'applicant_contact' => $payload['applicant_contact'],
                'applicant_BIRTHYEAR' => $payload['applicant_BIRTHYEAR'],
                'applicant_BIRTHMONTH' => $payload['applicant_BIRTHMONTH'],
                'applicant_BIRTHDAY' => $payload['applicant_BIRTHDAY'],
                'applicant_BIRTHPLACE' => $payload['applicant_BIRTHPLACE'],
                'applicant_GENDER' => $payload['applicant_GENDER'],
                'applicant_STREET' => $payload['applicant_STREET'],
                'applicant_BRGY' => $payload['applicant_BRGY'],
                'applicant_CITY' => $payload['applicant_CITY'],
                'applicant_DISTRICT' => $payload['applicant_DISTRICT'],
                'course_ID' => $payload['course_ID'],
                'applicant_PROVINCE' => $payload['applicant_PROVINCE'],
                'applicant_online_status' => $payload['applicant_online_status']
            ); 

           $check_email = $payload['applicant_email'];
           $this->db->where('applicant_email', $check_email); // to validate if the email already exists
           $query = $this->db->get('applicant');
           $num = $query->num_rows();

            if($num == 1){
               $response = array('status' => 'FAILED',
                                 'message' => 'USER DATA ALREADY EXISTS');
               echo json_encode($response);
            }

           elseif(!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $check_email)){
               $response = array('status' => 'FAILED',
                                 'message' => 'USER EMAIL IS INVALID');
               echo json_encode($response);
           }

           else{
               $this->db->insert('applicant', $data);
               $applicant_id =  $this->db->insert_id();
               $applicant_number = array(
                   'applicant_number' => str_pad($applicant_id, 3, '0', STR_PAD_LEFT)
               );

               $this->db->update('applicant', $applicant_number, array('applicant_id' => $applicant_id));

               $application = $this->db->get('application');
            //    $num_rows = $application->num_rows();

               $sum = str_pad($applicant_id, 3, '0', STR_PAD_LEFT);
               $application_num = 'APPLICATION#'.$sum;

               $this->db->where('acad_status', 1);
               $get = $this->db->get('academic_sem');

               $acad = $get->row();
               $data = array(
                   'acad_id' => $acad->acad_id,
                   'applicant_id' => $applicant_id,
                   'application_number'=> $application_num,
                   'application_payment_details' => "",
                   'application_exam_details' => "",
                   'application_date_released' => ""
               );

               $result = array(
                   'applicant_id' => $applicant_id
               );
               $sql = $this->db->insert("application", $data);
               $response = array(
                   'status'=>'SUCCESS',
                   'message'=>'SUCCESSFULLY INSERTED',
                   'payload'=>$result
               );
               echo json_encode($response);
           }
       }
       else{
           $response = array('status' => 'ERROR',
                             'message' => 'ERROR ENCOUNTERED');
           echo json_encode($response);
       }
    }
    
    public function submitAdditionalApplicantInfo($payload){
        if($payload == null){
            $response = array('status' => 'FAILED',
                              'message' => 'PLEASE CHECK YOUR DATA');
            echo json_encode($response);
        }
        
        elseif($payload != null){
            $data = array(
                'applicant_ID' => $payload['applicant_ID'],
                'aai_name_of_guardian' => $payload['aai_name_of_guardian'], 
                'aai_relationship' => $payload['aai_relationship'], 
                'aai_occupation' => $payload['aai_occupation'],
                'aai_tel_cell_number' => $payload['aai_tel_cell_number'],
                'aai_address' => $payload['aai_address'],
                'aai_elementary_graduated' => $payload['aai_elementary_graduated'],
                'aai_elementary_year_graduated' => $payload['aai_elementary_year_graduated'],
                'aai_highschool_graduated' => $payload['aai_highschool_graduated'],
                'aai_highschool_year_graduated' => $payload['aai_highschool_year_graduated'],
                'aai_college_last_attended' => $payload['aai_college_last_attended'],
                'aai_cla_inclusive_date' => $payload['aai_cla_inclusive_date'],
                'aai_degree_title' => $payload['aai_degree_title'],
                'aai_status' => $payload['aai_status'],
                'aai_graduating' => $payload['aai_graduating'],
                'aai_religion' => $payload['aai_religion'],
                'aai_civil' => $payload['aai_civil'],
                'aai_age' => $payload['aai_age'],
                'aai_height' => $payload['aai_height'],
                'aai_weight' => $payload['aai_weight'],
                'aai_special_skills' => $payload['aai_special_skills'],
                'aai_present_addr_st' => $payload['aai_present_addr_st'],
                'aai_present_brgy' => $payload['aai_present_brgy'],
                'aai_present_city' => $payload['aai_present_city'],
                'aai_present_district' => $payload['aai_present_district'],
                'aai_present_prov' => $payload['aai_present_prov'],
                'aai_present_contact' => $payload['aai_present_contact'],
            ); // to get the entered data
            
            $query = $this->db->get('applicant_additional_info');
            $num = $query->num_rows();
                
          // if($num == 1){
          //       $response = array('status' => 'SUCCESS',
          //                         'message' => 'USER DATA ALREADY EXISTS');
          //       echo json_encode($response);
          //   }
          //   else{

                $this->db->insert('applicant_additional_info', $data);
                $aai_ID =  $this->db->insert_id();
                $response = array('status' => 'SUCCESS',
                            'message' => 'SUCCESS INSERTING DATA',
                            'aai_ID' =>$aai_ID);

                echo json_encode($response);
            // }
        }
        else{
            $response = array('status' => 'ERROR',
                              'message' => 'ERROR ENCOUNTERED');
            echo json_encode($response);
        }
    }
    
    public function updateApplicantInfo($payload){
        if($payload == null){
            $response = array('status' => 'FAILED',
                              'message' => 'PLEASE CHECK YOUR DATA');
            echo json_encode($response);
        }
        
        elseif($payload != null){
            $this->db->where('applicant_id', $payload['applicant_id']);
            $data = $this->db->get('applicant');

            $num = $data->num_rows();
            
            if($num == 1){
              $deta = array(
                  'applicant_fname' => $payload['applicant_fname'],
                  'applicant_mname' => $payload['applicant_mname'], 
                  'applicant_lname' => $payload['applicant_lname'], 
                  'applicant_email' => $payload['applicant_email'],
                  'applicant_password' => base64_encode($payload['applicant_password']),
                  'applicant_address' => $payload['applicant_address'],
                  'applicant_contact' => $payload['applicant_contact'],
                  'applicant_BIRTHYEAR' => $payload['applicant_BIRTHYEAR'],
                  'applicant_BIRTHMONTH' => $payload['applicant_BIRTHMONTH'],
                  'applicant_BIRTHDAY' => $payload['applicant_BIRTHDAY'],
                  'applicant_BIRTHPLACE' => $payload['applicant_BIRTHPLACE'],
                  'applicant_GENDER' => $payload['applicant_GENDER'],
                  'applicant_STREET' => $payload['applicant_STREET'],
                  'applicant_BRGY' => $payload['applicant_BRGY'],
                  'applicant_CITY' => $payload['applicant_CITY'],
                  'applicant_DISTRICT' => $payload['applicant_DISTRICT'],
                  'applicant_PROVINCE' => $payload['applicant_PROVINCE'],
                  'applicant_online_status' => $payload['applicant_online_status']
              ); 
                $this->db->update('applicant', $deta, array('applicant_id' => $payload['applicant_id']));
                
                $response = array('status' => 'SUCCESS',
                                  'message' => 'SUCCESS UPDATING USER DATA');
                echo json_encode($response);
            }
            
            elseif($num == 0){
                $response = array('status' => 'FAILED',
                                  'message' => 'USER DOESN\'T EXIST');
                echo json_encode($response);
            }
            
            else{
                $response = array('status' => 'ERROR',
                                  'message' => 'ERROR ENCOUNTERED');
                echo json_encode($response);   
            }
        }
        
        else{
            $response = array('status' => 'ERROR',
                              'message' => 'ERROR ENCOUNTERED');
            echo json_encode($response);   
        }
    }
    
    public function updateAdditionalApplicantInfo($payload){
        if($payload == null){
            $response = array('status' => 'FAILED',
                              'message' => 'PLEASE CHECK YOUR DATA');
            echo json_encode($response);
        }
        
        elseif($payload != null){
            $this->db->where('aai_ID', $payload['aai_ID']);
            $data = $this->db->get('applicant_additional_info');

            $num = $data->num_rows();
            
            if($num == 1){
              $deta = array(
                  'applicant_ID' => $payload['applicant_ID'],
                  'aai_name_of_guardian' => $payload['aai_name_of_guardian'], 
                  'aai_relationship' => $payload['aai_relationship'], 
                  'aai_occupation' => $payload['aai_occupation'],
                  'aai_tel_cell_number' => $payload['aai_tel_cell_number'],
                  'aai_address' => $payload['aai_address'],
                  'aai_elementary_graduated' => $payload['aai_elementary_graduated'],
                  'aai_elementary_year_graduated' => $payload['aai_elementary_year_graduated'],
                  'aai_highschool_graduated' => $payload['aai_highschool_graduated'],
                  'aai_highschool_year_graduated' => $payload['aai_highschool_year_graduated'],
                  'aai_college_last_attended' => $payload['aai_college_last_attended'],
                  'aai_cla_inclusive_date' => $payload['aai_cla_inclusive_date'],
                  'aai_degree_title' => $payload['aai_degree_title'],
                  'aai_status' => $payload['aai_status'],
                  'aai_graduating' => $payload['aai_graduating'],
                  'aai_religion' => $payload['aai_religion'],
                  'aai_civil' => $payload['aai_civil'],
                  'aai_age' => $payload['aai_age'],
                  'aai_height' => $payload['aai_height'],
                  'aai_weight' => $payload['aai_weight'],
                  'aai_special_skills' => $payload['aai_special_skills'],
                  'aai_present_addr_st' => $payload['aai_present_addr_st'],
                  'aai_present_brgy' => $payload['aai_present_brgy'],
                  'aai_present_city' => $payload['aai_present_city'],
                  'aai_present_district' => $payload['aai_present_district'],
                  'aai_present_prov' => $payload['aai_present_prov'],
                  'aai_present_contact' => $payload['aai_present_contact'],
              ); 
                $this->db->update('applicant_additional_info', $deta, array('aai_ID' => $payload['aai_ID']));
                
                $response = array('status' => 'SUCCESS',
                                  'message' => 'SUCCESS UPDATING USER DATA');
                echo json_encode($response);
            }
            
            elseif($num == 0){
                $response = array('status' => 'FAILED',
                                  'message' => 'USER DOESN\'T EXIST');
                echo json_encode($response);
            }
            
            else{
                $response = array('status' => 'ERROR',
                                  'message' => 'ERROR ENCOUNTERED');
                echo json_encode($response);   
            }
        }
        
        else{
            $response = array('status' => 'ERROR',
                              'message' => 'ERROR ENCOUNTERED');
            echo json_encode($response);   
        }
    }
   
    public function fetchApplicantInfoByApplicantStatus($payload){ 
     $this->db->where('applicant_status', $payload['applicant_status']);
           $query = $this->db->get('applicant');
         
           $result = $query->result();
           
           if($query->num_rows() == 1){
              $response = array('status' => 'SUCCESS',
                                'message' => 'SUCCESS FETCHING DATA',
                                 'payload' => $result); // if success
             
              echo json_encode($response);
           }
           else{
              $response = array('status' => 'FAILED',
                                'message' => 'FAILED FETCHING DATA'); // if failed
              echo json_encode($response);
          }
    }
   
    public function fetchApplicantInfoByApplicantID($payload){ 
     $this->db->where('applicant_ID', $payload['applicant_ID']);
           $query = $this->db->get('applicant');
         
           $result = $query->row();
           
           if($query->num_rows() == 1){
              $response = array('status' => 'SUCCESS',
                                'message' => 'SUCCESS FETCHING DATA',
                                 'payload' => $result); // if success
             
              echo json_encode($response);
           }
           else{
              $response = array('status' => 'FAILED',
                                'message' => 'FAILED FETCHING DATA'); // if failed
              echo json_encode($response);
          }
   }
   
    public function fetchAdditionalApplicantInfoByID($payload){ //department_list
     $this->db->where('aai_ID', $payload['aai_ID']);
           $query = $this->db->get('applicant_additional_info');
         
           $result = $query->row();
           
           if($query->num_rows() == 1){
              $response = array('status' => 'SUCCESS',
                                'message' => 'SUCCESS FETCHING DATA',
                                 'payload' => $result); // if success
             
              echo json_encode($response);
           }
           else{
              $response = array('status' => 'FAILED',
                                'message' => 'FAILED FETCHING DATA'); // if failed
              echo json_encode($response);
          }
   }
    
    public function updatePassword($payload){
        if($payload == null){
            $response = array('status' => 'FAILED',
                              'message' => 'PLEASE CHECK YOUR DATA');
            echo json_encode($response);
        }
        
        elseif($payload != null){
            $this->db->where('applicant_id', $payload['applicant_id']);
            $data = $this->db->get('applicant');
            
            $num = $data->num_rows();
            
            if($num == 1){
                $deta = array('applicant_password' => base64_encode($payload['applicant_password']));
                $this->db->update('applicant', $deta, array('applicant_id' => $payload['applicant_id']));
                
                $response = array('status' => 'SUCCESS',
                                  'message' => 'SUCCESS UPDATING PASSWORD');
                echo json_encode($response);
            }
            
            elseif($num == 0){
                $response = array('status' => 'FAILED',
                                  'message' => 'USER DOESN\'T EXIST');
                echo json_encode($response);
            }
            
            else{
                $response = array('status' => 'ERROR',
                                  'message' => 'ERROR ENCOUNTERED');
                echo json_encode($response);   
            }
        }
        
        else{
            $response = array('status' => 'ERROR',
                              'message' => 'ERROR ENCOUNTERED');
            echo json_encode($response);   
        }
    }
   
    public function updateApplicantStatus($payload){
        if($payload == null){
            $response = array('status' => 'FAILED',
                              'message' => 'PLEASE CHECK YOUR DATA');
            echo json_encode($response);
        }
        
        elseif($payload != null){
            $this->db->where('applicant_id', $payload['applicant_id']);
            $data = $this->db->get('applicant');
            
            $num = $data->num_rows();
            
            if($num == 1){
                $deta = array('applicant_status' => 1);
                $this->db->update('applicant', $deta, array('applicant_id' => $payload['applicant_id']));
                
                $response = array('status' => 'SUCCESS',
                                  'message' => 'SUCCESS UPDATING STATUS');
                echo json_encode($response);
            }
            
            elseif($num == 0){
                $response = array('status' => 'FAILED',
                                  'message' => 'USER DOESN\'T EXIST');
                echo json_encode($response);
            }
            
            else{
                $response = array('status' => 'ERROR',
                                  'message' => 'ERROR ENCOUNTERED');
                echo json_encode($response);   
            }
        }
        
        else{
            $response = array('status' => 'ERROR',
                              'message' => 'ERROR ENCOUNTERED');
            echo json_encode($response);   
        }
    }
   
    public function updateApplicantStatus1($payload){
        if($payload == null){
            $response = array('status' => 'FAILED',
                              'message' => 'PLEASE CHECK YOUR DATA');
            echo json_encode($response);
        }
        
        elseif($payload != null){
            $this->db->where('applicant_id', $payload['applicant_id']);
            $data = $this->db->get('applicant');
            
            $num = $data->num_rows();
            
            if($num == 1){
                $deta = array(
                    'applicant_status' => $payload['applicant_status']
                );
                $this->db->update('applicant', $deta, array('applicant_id' => $payload['applicant_id']));
                
                $response = array('status' => 'SUCCESS',
                                  'message' => 'SUCCESS UPDATING APPLICATION DATA',
                                  'payload' => $payload);
                echo json_encode($response);
            }
            
            elseif($num == 0){
                $response = array('status' => 'FAILED',
                                  'message' => 'APPLICATION DOESN\'T EXIST');
                echo json_encode($response);
            }
            
            else{
                $response = array('status' => 'ERROR',
                                  'message' => 'ERROR ENCOUNTERED');
                echo json_encode($response);   
            }
        }
        
        else{
            $response = array('status' => 'ERROR',
                              'message' => 'ERROR ENCOUNTERED');
            echo json_encode($response);   
        }
    }
   
    public function getApplicantList(){
        $this->db->select('*');
        $this->db->from('application');
        $this->db->join('applicant', 'applicant.applicant_id = application.applicant_id');
        $sql = $this->db->get();
        
		if ($sql) {
			if ($sql->num_rows() > 0) {
				$data = $sql->result();
				$response = array(
					'status' => 'SUCCESS',
					'message' => 'SUCCESS FETCHING DATA',
					'payload' => $data
				);
				
				return json_encode($response);
			}
			else {
				$response = array(
					'status' => 'FAILED',
					'message' => 'FAILED RETRIEVING DATA'
				);
				return json_encode($response);
			}
		}
		else {
			$response = array(
				'status' => 'ERROR',
				'message' => 'ERROR'
			);
			return json_encode($response);
			$error = $this->db->error();
			print_r($error);
		}
	}
    
    public function getApplicantById($payload){
        $this->db->where('applicant_id', $payload['applicant_id']);
        $sql = $this->db->get('applicant'); //table of workgroups
		if ($sql) {
			if ($sql->num_rows() > 0) {
				$data = $sql->row();
				$response = array(
					'status' => 'SUCCESS',
					'message' => 'SUCCESS FETCHING DATA',
					'payload' => $data
				);
				
				echo json_encode($response);
			}
			else {
				$response = array(
					'status' => 'FAILED',
					'message' => 'FAILED RETRIEVING DATA'
				);
				echo json_encode($response);
			}
		}
		else {
			$response = array(
				'status' => 'ERROR',
				'message' => 'ERROR'
			);
			echo json_encode($response);
			$error = $this->db->error();
			print_r($error);
		}
    }

    public function getApplicantByNumber($payload){
        $this->db->select("*");
        $this->db->from("exam_result er");
        $this->db->join("application app", "app.application_id = er.application_id");
        $this->db->where('app.application_number', $payload['application_number']);
        $sql = $this->db->get();
        if($sql->num_rows() > 0){
            foreach($sql->result() as $row){   
               if($row->exam_id == 1){
                   $applicant = $this->db->get_where("applicant", array("applicant_id"=>$row->applicant_id));
                   foreach($applicant->result() as $row1){
                       $iq = array(
                           'iq_id'=>$row->exam_result_id,
                           'application_id'=>$row->application_id,
                           'fname'=>$row1->applicant_fname.' '.$row1->applicant_lname,
                           'application_number'=>$row->application_number,
                           'IQ'=>$row->questionnaire_result,
                           'application_status'=>$row->application_status
                       );   
                   }
                   
               }
               elseif($row->exam_id == 2){
                   $exp = explode(",", $row->questionnaire_result);
                   $eq = array(
                       'eq_id'=>$row->exam_result_id,
                       'A'=>$exp[0],
                       'B'=>$exp[1],
                       'C'=>$exp[2],
                       'D'=>$exp[3],
                       'E'=>$exp[4],
                       'SUM'=>$exp[5],
                       'QUO'=>$exp[6]
                   );
                   $data[] = array_merge($iq,$eq);
                   $response = array(
                               'status'=>'SUCCESS',
                               'message'=>'SUCCESS',
                               'payload'=>$data
                   );
               }
           }
           echo json_encode($response);
       }
        
       elseif($sql->num_rows() < 1){
           $this->db->where('application_number', $payload['application_number']);
           $get = $this->db->get('application');

           if($get->num_rows() > 0){
                $row = $get->row();
                $this->db->where('applicant_id', $row->applicant_id);
                $get_applicant = $this->db->get('applicant');
                $row_applicant = $get_applicant->row();

                $output[] = array(
                    'application_id' => $row->application_id,
                    'fname' => $row_applicant->applicant_fname.' '.$row_applicant->applicant_lname
                );

                $response = array(
                    'status' => 'SUCCESS',
                    'message' => 'SUCCESS FETCHING DATA',
                    'payload' => $output
                );

                echo json_encode($response);
           }
           else{
                $response = array(
                    'status' => 'FAILED',
                    'message' => 'APPLICANT DOES NOT EXIST'
                );
                echo json_encode($response);
           }
       }    
        
       else{
           $response = array(
                       'status'=>'ERROR',
                       'message'=>'ERROR'
           );
           echo json_encode($response);
       }
    }
    
    public function login($payload){
        $this->db->where('applicant_email', $payload['applicant_email']);
        $this->db->where('applicant_password', base64_encode($payload['applicant_password']));
        $sql = $this->db->get('applicant'); //table of workgroups
    
		if ($sql) {
			if ($sql->num_rows() > 0) {
				$data = $sql->row();
                $result = array('applicant_id' => $data->applicant_id);
                
				$response = array(
					'status' => 'SUCCESS',
					'message' => 'SUCCESS LOGIN',
                    'payload' => $result
				);
				
				echo json_encode($response);
			}
			else {
				$response = array(
					'status' => 'FAILED',
					'message' => 'FAILED TO LOGIN'
				);
				echo json_encode($response);
			}
		}
		else {
			$response = array(
				'status' => 'ERROR',
				'message' => 'ERROR'
			);
			echo json_encode($response);
			$error = $this->db->error();
			print_r($error);
		}
    }
    
    public function submitApplication($payload){
        $sql2 = $this->db->query("SELECT application_id FROM application");
        if($sql2){
            $num_rows = $sql2->num_rows();
            $sum = $num_rows + 1;
            $application_num = 'APPLICATION#'.$sum;

            $acad_id = $payload['acad_id'];
            $applicant_id = $payload['applicant_id'];
            $sql = $this->db->query("SELECT acad_id FROM application WHERE applicant_id = '$applicant_id' AND acad_id = '$acad_id'");
            //$sql =$this->db->query("SELECT * FROM application WHERE acad_id = '$acad_id'");
            $sql2= $this->db->query("SELECT * FROM applicant WHERE applicant_id= '$applicant_id'");
            
            if($sql2->num_rows() > 0){
                if($sql->num_rows() > 0){
                    $response = array(
                        'status'=>'ERROR',
                        'message'=>'ACAD_ID ALREADY EXISTS'
                    );
                }
                else{
                    $data = array(
                        'acad_id' => $acad_id,
                        'applicant_id' => $applicant_id,
                        'application_number'=>$application_num,
                        'application_payment_details' => $payload['application_payment_details'],
                        'application_exam_details' => $payload['application_exam_details'],
                        'application_date_released' => $payload['application_date_released']
                    );
                    $sql = $this->db->insert("application", $data);
                    $response = array(
                        'status'=>'SUCCESS',
                        'message'=>'SUCCESSFULLY INSERTED',
                        'payload'=>$data
                    );
                }
            }
            else{
                $response = array(
                        'status'=>'ERROR',
                        'message'=>'APPLICANT_ID DOES NOT EXIST'
                );
            }
            echo json_encode($response);
        }
	}
    
    public function displayVoucherInfo($payload){
        $this->db->select("*,DATE_FORMAT(application_exam_details, '%M %d,  %Y %h:%i %p') as date");
        $this->db->from("application");
        $this->db->where("application_id",$payload['application_id']);
        $sql = $this->db->get();
        if ($sql) {
			if ($sql->num_rows() > 0) {
				$data = $sql->row();
                
                $this->db->where('applicant_id', $data->applicant_id);
                $deta = $this->db->get('applicant');
                $result = $deta->row();
                $flag = "";
                if($data->application_status == "6"){
                    $flag = "Paid";
                }else{
                    $flag = "Pending";
                }
                
                $sql = $this->db->query("SELECT CURDATE(), DATE_FORMAT(application_exam_details,'%Y-%m-%d'), DATEDIFF(CURDATE(),DATE_FORMAT(application_exam_details,'%Y-%m-%d')) as DAYCOUNT FROM application WHERE application_id ='".$payload['application_id']."'");
                
                $row = $sql->row();
                $daycount = $row->DAYCOUNT;
                if($daycount == 0){
                    $status = 'YES';
                }
                else{
                    $status = 'NO';
                }
                
                $display = array(
                    'application_id' => $data->application_id,
                    'applicant_name' => $result->applicant_fname.' '.$result->applicant_lname,
                    'application_number' => $data->application_number,
                    'payment_status' => $flag,
                    'exam_details' => $data->application_date_released,
                    'examination_datetime' => $data->application_exam_details,
                    'application_exam_details' => $data->date,
                    'exam_status' => $status
                );
				$response = array(
					'status' => 'SUCCESS',
					'message' => 'SUCCESS FETCHING DATA',
					'payload' => $display
				);
				
				echo json_encode($response);
			}
			else {
				$response = array(
					'status' => 'FAILED',
					'message' => 'FAILED RETRIEVING DATA'
				);
				echo json_encode($response);
			}
		}
		else {
			$response = array(
				'status' => 'ERROR',
				'message' => 'ERROR'
			);
			echo json_encode($response);
			$error = $this->db->error();
			print_r($error);
		} 
    }
    
    public function updateExamSchedule($payload){
        if($payload == null){
            $response = array('status' => 'FAILED',
                              'message' => 'PLEASE CHECK YOUR DATA');
            echo json_encode($response);
        }
        
        elseif($payload != null){
            $this->db->where('application_id', $payload['application_id']);
            $data = $this->db->get('application');
            
            $num = $data->num_rows();
            
            if($num == 1){
                $deta = array(
                    'application_exam_details' => $payload['application_exam_details']
                );
                $this->db->update('application', $deta, array('application_id' => $payload['application_id']));
                
                $response = array('status' => 'SUCCESS',
                                  'message' => 'SUCCESS UPDATING APPLICATION DATA',
                                  'payload' => $payload);
                echo json_encode($response);
            }
            
            elseif($num == 0){
                $response = array('status' => 'FAILED',
                                  'message' => 'APPLICATION DOESN\'T EXIST');
                echo json_encode($response);
            }
            
            else{
                $response = array('status' => 'ERROR',
                                  'message' => 'ERROR ENCOUNTERED');
                echo json_encode($response);   
            }
        }
        
        else{
            $response = array('status' => 'ERROR',
                              'message' => 'ERROR ENCOUNTERED');
            echo json_encode($response);   
        }
    }

    public function updateInterviewSchedule($payload){
        if($payload == null){
            $response = array('status' => 'FAILED',
                              'message' => 'PLEASE CHECK YOUR DATA');
            echo json_encode($response);
        }
        
        elseif($payload != null){
            $this->db->where('application_id', $payload['application_id']);
            $data = $this->db->get('application');
            
            $num = $data->num_rows();
            
            if($num == 1){
                $deta = array(
                    'application_interview_sched' => $payload['application_interview_sched']
                );
                $this->db->update('application', $deta, array('application_id' => $payload['application_id']));
                
                $response = array('status' => 'SUCCESS',
                                  'message' => 'SUCCESS UPDATING APPLICATION DATA',
                                  'payload' => $payload);
                echo json_encode($response);
            }
            
            elseif($num == 0){
                $response = array('status' => 'FAILED',
                                  'message' => 'APPLICATION DOESN\'T EXIST');
                echo json_encode($response);
            }
            
            else{
                $response = array('status' => 'ERROR',
                                  'message' => 'ERROR ENCOUNTERED');
                echo json_encode($response);   
            }
        }
        
        else{
            $response = array('status' => 'ERROR',
                              'message' => 'ERROR ENCOUNTERED');
            echo json_encode($response);   
        }
    }

    public function updateInterviewResult($payload){
        if($payload == null){
            $response = array('status' => 'FAILED',
                              'message' => 'PLEASE CHECK YOUR DATA');
            echo json_encode($response);
        }
        
        elseif($payload != null){
            $this->db->where('application_id', $payload['application_id']);
            $data = $this->db->get('application');
            
            $num = $data->num_rows();
            
            if($num == 1){
                $deta = array(
                    'application_interview_result' => $payload['application_interview_result']
                );
                $this->db->update('application', $deta, array('application_id' => $payload['application_id']));
                
                $response = array('status' => 'SUCCESS',
                                  'message' => 'SUCCESS UPDATING APPLICATION DATA',
                                  'payload' => $payload);
                echo json_encode($response);
            }
            
            elseif($num == 0){
                $response = array('status' => 'FAILED',
                                  'message' => 'APPLICATION DOESN\'T EXIST');
                echo json_encode($response);
            }
            
            else{
                $response = array('status' => 'ERROR',
                                  'message' => 'ERROR ENCOUNTERED');
                echo json_encode($response);   
            }
        }
        
        else{
            $response = array('status' => 'ERROR',
                              'message' => 'ERROR ENCOUNTERED');
            echo json_encode($response);   
        }
    }

    public function updateBoaSchedule($payload){
        if($payload == null){
            $response = array('status' => 'FAILED',
                              'message' => 'PLEASE CHECK YOUR DATA');
            echo json_encode($response);
        }
        
        elseif($payload != null){
            $this->db->where('application_id', $payload['application_id']);
            $data = $this->db->get('application');
            
            $num = $data->num_rows();
            
            if($num == 1){
                $deta = array(
                    'application_boa_sched' => $payload['application_boa_sched']
                );
                $this->db->update('application', $deta, array('application_id' => $payload['application_id']));
                
                $response = array('status' => 'SUCCESS',
                                  'message' => 'SUCCESS UPDATING APPLICATION DATA',
                                  'payload' => $payload);
                echo json_encode($response);
            }
            
            elseif($num == 0){
                $response = array('status' => 'FAILED',
                                  'message' => 'APPLICATION DOESN\'T EXIST');
                echo json_encode($response);
            }
            
            else{
                $response = array('status' => 'ERROR',
                                  'message' => 'ERROR ENCOUNTERED');
                echo json_encode($response);   
            }
        }
        
        else{
            $response = array('status' => 'ERROR',
                              'message' => 'ERROR ENCOUNTERED');
            echo json_encode($response);   
        }
    }

    public function updateBoaResult($payload){
        if($payload == null){
            $response = array('status' => 'FAILED',
                              'message' => 'PLEASE CHECK YOUR DATA');
            echo json_encode($response);
        }
        
        elseif($payload != null){
            $this->db->where('application_id', $payload['application_id']);
            $data = $this->db->get('application');
            
            $num = $data->num_rows();
            
            if($num == 1){
                $deta = array(
                    'application_boa_result' => $payload['application_boa_result']
                );
                $this->db->update('application', $deta, array('application_id' => $payload['application_id']));
                
                $response = array('status' => 'SUCCESS',
                                  'message' => 'SUCCESS UPDATING APPLICATION DATA',
                                  'payload' => $payload);
                echo json_encode($response);
            }
            
            elseif($num == 0){
                $response = array('status' => 'FAILED',
                                  'message' => 'APPLICATION DOESN\'T EXIST');
                echo json_encode($response);
            }
            
            else{
                $response = array('status' => 'ERROR',
                                  'message' => 'ERROR ENCOUNTERED');
                echo json_encode($response);   
            }
        }
        
        else{
            $response = array('status' => 'ERROR',
                              'message' => 'ERROR ENCOUNTERED');
            echo json_encode($response);   
        }
    }

    public function updateMedicalSchedule($payload){
        if($payload == null){
            $response = array('status' => 'FAILED',
                              'message' => 'PLEASE CHECK YOUR DATA');
            echo json_encode($response);
        }
        
        elseif($payload != null){
            $this->db->where('application_id', $payload['application_id']);
            $data = $this->db->get('application');
            
            $num = $data->num_rows();
            
            if($num == 1){
                $deta = array(
                    'application_medical_sched' => $payload['application_medical_sched']
                );
                $this->db->update('application', $deta, array('application_id' => $payload['application_id']));
                
                $response = array('status' => 'SUCCESS',
                                  'message' => 'SUCCESS UPDATING APPLICATION DATA',
                                  'payload' => $payload);
                echo json_encode($response);
            }
            
            elseif($num == 0){
                $response = array('status' => 'FAILED',
                                  'message' => 'APPLICATION DOESN\'T EXIST');
                echo json_encode($response);
            }
            
            else{
                $response = array('status' => 'ERROR',
                                  'message' => 'ERROR ENCOUNTERED');
                echo json_encode($response);   
            }
        }
        
        else{
            $response = array('status' => 'ERROR',
                              'message' => 'ERROR ENCOUNTERED');
            echo json_encode($response);   
        }
    }

    public function updateMedicalResult($payload){
        if($payload == null){
            $response = array('status' => 'FAILED',
                              'message' => 'PLEASE CHECK YOUR DATA');
            echo json_encode($response);
        }
        
        elseif($payload != null){
            $this->db->where('application_id', $payload['application_id']);
            $data = $this->db->get('application');
            
            $num = $data->num_rows();
            
            if($num == 1){
                $deta = array(
                    'application_medical_result' => $payload['application_medical_result']
                );
                $this->db->update('application', $deta, array('application_id' => $payload['application_id']));
                
                $response = array('status' => 'SUCCESS',
                                  'message' => 'SUCCESS UPDATING APPLICATION DATA',
                                  'payload' => $payload);
                echo json_encode($response);
            }
            
            elseif($num == 0){
                $response = array('status' => 'FAILED',
                                  'message' => 'APPLICATION DOESN\'T EXIST');
                echo json_encode($response);
            }
            
            else{
                $response = array('status' => 'ERROR',
                                  'message' => 'ERROR ENCOUNTERED');
                echo json_encode($response);   
            }
        }
        
        else{
            $response = array('status' => 'ERROR',
                              'message' => 'ERROR ENCOUNTERED');
            echo json_encode($response);   
        }
    }
    
    public function updateApplicationStatus($payload){
        if($payload == null){
            $response = array('status' => 'FAILED',
                              'message' => 'PLEASE CHECK YOUR DATA');
            echo json_encode($response);
        }
        
        elseif($payload != null){
            $this->db->where('application_id', $payload['application_id']);
            $data = $this->db->get('application');
            
            $num = $data->num_rows();
            
            if($payload['application_status'] > 7){
                $response = array('status' => 'FAILED',
                                  'message' => 'INVALID APPLICATION STATUS');
                echo json_encode($response);
            }
            elseif($num == 1){
                $deta = array('application_status' => $payload['application_status']);
                $this->db->update('application', $deta, array('application_id' => $payload['application_id']));
                
                $response = array('status' => 'SUCCESS',
                                  'message' => 'SUCCESS UPDATING STATUS');
                echo json_encode($response);
            }
            
            else{
                $response = array('status' => 'FAILED',
                                  'message' => 'DATA DOES NOT EXIST');
                echo json_encode($response);   
            }
        }
        
        else{
            $response = array('status' => 'ERROR',
                              'message' => 'ERROR ENCOUNTERED');
            echo json_encode($response);   
        }
    }
    
	public function submitExamTemplate($payload){
        if($payload == null){
            $response = array('status' => 'FAILED',
                              'message' => 'PLEASE CHECK YOUR DATA');
            echo json_encode($response);
        }
        
        elseif($payload != null){
            $data = array(
                'exam_name'=>$payload['exam_name'],
				'exam_passing_score'=>$payload['exam_passing_score']
		    );
            
            $this->db->where('exam_name', $payload['exam_name']);
            $check = $this->db->get('exam_template');
            
            $num = $check->num_rows();
            
		    if($num > 0){
                $response = array(
                    'status'=>'ERROR',
					'message'=>'TEMPLATE ALREADY EXISTS'
			    );
                
                echo json_encode($response);
            }
            
            else{
                $this->db->insert('exam_template', $data);
                $response = array(
                    'status'=>'SUCCESS',
					'message'=>'TEMPLATE SUCCESSFULLY SUBMITTED'
			    );
                
                echo json_encode($response);
            }
        }
        
        else{
            $response = array(
                    'status'=>'ERROR',
					'message'=>'ERROR'
            );
                
            echo json_encode($response);
        }
    }
    
    public function submitExamTemplateQuestion($payload){

        $sql = $this->db->get_where('exam_template', array('exam_id'=>$payload['exam_id']));
        $sql2 = $this->db->get_where('questionnaire_bank', array('questionnaire_id'=> $payload['questionnaire_id']));

        if($sql->num_rows() > 0    ){
            if($sql2->num_rows() > 0 ){
                $data = array(
                    'exam_id'=>$payload['exam_id'],
                    'questionnaire_id'=>$payload['questionnaire_id']
                );
                $sql3 = $this->db->insert("exam_template_questions", $data);
                $response = array(
                            'status'=>'SUCCESS',
                            'message'=>'SUCCESS',
                            'payload'=>$data
                );
            }else{
                $response = array(
                            'status'=>'ERROR',
                            'message'=>'QUESTIONNAIRE ID IS NOT EXISTING'
            );
            }
        }else{
            $response = array(
                        'status'=>'ERROR',
                        'message'=>'EXAM ID IS NOT EXISTING'
            );
        }
        echo json_encode($response);
    }
    
    public function generateAnswerID($payload){
        if($payload == null){
            $response = array('status' => 'FAILED',
                              'message' => 'PLEASE CHECK YOUR DATA');
            echo json_encode($response);
        }
        
        elseif($payload != null){
            $this->db->where('application_id', $payload['application_id']);
            $sql = $this->db->get('application');
            
            if($sql->num_rows() > 0){
                $data = array(
                    'answer' => null,
                    'exam_template_questions_id' => null,
                    'application_id' => $payload['application_id'],
                    'exam_id' => 1
                );
                        
                $this->db->insert('answer_bank', $data);
                $id = $this->db->insert_id();
                            
                $payload = array(
                    'answer_id'=>$id,             
                    'answer' => null,
                    'exam_template_questions_id' => null,
                    'application_id' => $payload['application_id'],
                    'exam_id' => 1
                );
                            
                $response = array(
                    'status' => 'SUCCESS',
                    'message' => 'ANSWER SUBMITTED',
                    'payload' => $payload
                );
                
                echo json_encode($response);            
            } 
            
            elseif($sql->num_rows() < 1){
                $response = array(
                    'status' => 'FAILED',
                    'message' => 'APPLICATION DOESN\'T EXIST'
                );
                echo json_encode($response);
            }
            
            else{
                $response = array(
                    'status' => 'ERROR',
                    'message' => 'ERROR'
                );
                echo json_encode($response);    
            }
        }
        
        else{
            $response = array('status' => 'ERROR',
                              'message' => 'ERROR');
            echo json_encode($response);
        }
    }
    
    // public function submitIQ($payload){
    //     if($payload == null){
    //         $response = array('status' => 'FAILED',
    //                           'message' => 'PLEASE CHECK YOUR DATA');
    //         echo json_encode($response);
    //     }
    
    //     elseif($payload != null){
    //         if($payload['answer_id'] != null){
    //             $this->db->where('answer_id', $payload['answer_id']);
    //             $sql = $this->db->get('answer_bank');
                
    //             $this->db->where('questionnaire_id', $payload['questionnaire_id']);
    //             $this->db->where('exam_id', 1);
    //             $get = $this->db->get('exam_template_questions');
                
    //             if($sql->num_rows() > 0){
    //                 if($get->num_rows() > 0){
    //                     $etq = $get->row();
    //                     $data = $sql->row();
                            
    //                     $this->db->where('questionnaire_id', $payload['questionnaire_id']);
    //                     $get1 = $this->db->get('questionnaire_bank');
                        
    //                     $qb = $get1->row();
                        
                        // if($data->answer == null && $data->exam_template_questions_id == null && $payload['answer'] == $qb->questionnaire_answer){
                        //     $insert = array(
                        //         'answer' => $payload['answer'],
                        //         'exam_template_questions_id' => $etq->exam_template_questions_id
                        //     );
                        //     $this->db->update('answer_bank', $insert, array('answer_id' => $payload['answer_id']));
                            
                        //     $this->db->where('answer_id', $payload['answer_id']);
                        //     $sql2 = $this->db->get('answer_bank');

                        //     $deta = $sql2->row();
                            
                        //     $response = array(
                        //         'status' => 'SUCCESS',
                        //         'message' => 'ANSWER SUBMITTED',
                        //         'payload' => $deta
                        //     );
                        //     echo json_encode($response);
                        // }
                        
                        // elseif($data->answer != null && $data->exam_template_questions_id != null && $payload['answer'] == $qb->questionnaire_answer){
                        //     $this->db->query("UPDATE answer_bank SET answer = '".$data->answer.",".$payload['answer']."' WHERE answer_id = '".$payload['answer_id']."'");

                        //     $this->db->query("UPDATE answer_bank SET exam_template_questions_id = '".$data->exam_template_questions_id.",".$etq->exam_template_questions_id."' WHERE answer_id = '".$payload['answer_id']."'");

                        //     $this->db->where('answer_id', $payload['answer_id']);
                        //     $sql2 = $this->db->get('answer_bank');

                        //     $deta = $sql2->row();

                        //     $response = array(
                        //         'status' => 'SUCCESS',
                        //         'message' => 'ANSWER SUBMITTED',
                        //         'payload' => $deta
                        //     );
                        //     echo json_encode($response);
                        // }
                        // else{
                        //     $response = array(
                        //         'status' => 'FAILED',
                        //         'message' => 'WRONG ANSWER'
                        //     );
                        //     echo json_encode($response);
                        // }
    //                 }
    //             }
                
    //             else{
    //                 $response = array(
    //                     'status' => 'ERROR',
    //                     'message' => 'ERROR ENCOUNTERED');
    //                 echo json_encode($response);
    //             }
    //         }
    //     }
            
    //     else{
    //         $response = array('status' => 'ERROR',
    //                           'message' => 'ERROR ENCOUNTERED');
    //         echo json_encode($response);
    //     }
    // }

    public function submitIQ($payload){
        if($payload == null){
            $response = array('status' => 'FAILED',
                              'message' => 'PLEASE CHECK YOUR DATA');
            echo json_encode($response);
        }
    
        elseif($payload != null){
            $this->db->where('answer_id', $payload['answer_id']);
            $get_bank = $this->db->get('answer_bank');

            if($get_bank->num_rows() > 0){
                $row_bank = $get_bank->row();

                $this->db->select('*');
                $this->db->from('exam_template_questions etq');
                $this->db->join('exam_template et', 'et.exam_id = etq.exam_id');
                $this->db->join('questionnaire_bank qb', 'qb.questionnaire_id = etq.questionnaire_id');
                $this->db->where('etq.questionnaire_id', $payload['questionnaire_id']);
                $get_question = $this->db->get();
                $row_question = $get_question->row();

                if($row_bank->answer == null && $row_bank->exam_template_questions_id == null && $payload['answer'] == $row_question->questionnaire_answer){
                    $insert = array(
                        'answer' => $payload['answer'],
                        'exam_template_questions_id' => $row_question->exam_template_questions_id
                    );
                    $this->db->update('answer_bank', $insert, array('answer_id' => $payload['answer_id']));
                    
                    $this->db->where('answer_id', $payload['answer_id']);
                    $sql2 = $this->db->get('answer_bank');

                    $output = $sql2->row();
                    
                    $response = array(
                        'status' => 'SUCCESS',
                        'message' => 'ANSWER SUBMITTED',
                        'payload' => $output
                    );
                    echo json_encode($response);
                }
                elseif($row_bank->answer != null && $row_bank->exam_template_questions_id != null && $payload['answer'] == $row_question->questionnaire_answer){
                    $this->db->query("UPDATE answer_bank SET answer = '".$row_bank->answer.",".$payload['answer']."' WHERE answer_id = '".$payload['answer_id']."'");

                    $this->db->query("UPDATE answer_bank SET exam_template_questions_id = '".$row_bank->exam_template_questions_id.",".$row_question->exam_template_questions_id."' WHERE answer_id = '".$payload['answer_id']."'");

                    $this->db->where('answer_id', $payload['answer_id']);
                    $sql2 = $this->db->get('answer_bank');

                    $deta = $sql2->row();

                    $response = array(
                        'status' => 'SUCCESS',
                        'message' => 'ANSWER SUBMITTED',
                        'payload' => $deta
                    );
                    echo json_encode($response);
                }
                else{
                    $response = array(
                        'status' => 'FAILED',
                        'message' => 'WRONG ANSWER'
                    );
                    echo json_encode($response);
                }
            }
            else{
                $response = array(
                    'status' => 'FAILED',
                    'message' => 'NO DATA FOUND'
                );
            echo json_encode($response);
            }
        }
            
        else{
            $response = array('status' => 'ERROR',
                              'message' => 'ERROR ENCOUNTERED');
            echo json_encode($response);
        }
    }
    
    public function submitEQ($payload){
    	if($payload == null){
    		$response = array(
    					'status'=>'FAILED',
    					'message'=>'PLEASE CHECK YOUR DATA'
    		);
    		echo json_encode($response);
    	}
        else{
            $this->db->where('answer_id', $payload['answer_id']);
            $sql = $this->db->get('answer_bank');
            $row = $sql->row();
            
    		if($sql->num_rows() > 0){
                
                if($row->answer == null && $row->exam_template_questions_id == null){
                    $sql = $this->db->get_where("application", array("application_id"=>$payload['application_id']));
                    
                    if($sql->num_rows() > 0){
                        $this->db->where('questionnaire_id', $payload['questionnaire_id']);
                        $this->db->where('exam_id', 2);
                        $get = $this->db->get('exam_template_questions');
                        if($get->num_rows() > 0){
                            $etq = $get->row();
                            $this->db->where('questionnaire_id', $payload['questionnaire_id']);
                            $get1 = $this->db->get('questionnaire_bank');
                            $qb = $get1->row();
                            
                            if($qb->questionnaire_answer_group == 'A'){
                                $answer = $payload['answer'].',,,,';
                            }elseif($qb->questionnaire_answer_group == 'B'){
                                $answer = ','.$payload['answer'].',,,';

                            }elseif($qb->questionnaire_answer_group == 'C'){
                                $answer = ',,'.$payload['answer'].',,';

                            }elseif($qb->questionnaire_answer_group == 'D'){
                                $answer = ',,,'.$payload['answer'].',';

                            }elseif($qb->questionnaire_answer_group == 'E'){
                                $answer = ',,,,'.$payload['answer'];
                            }elseif($qb->questionnaire_answer_group == 'G'){
                                $payload['answer'] = 0;
                                $answer = $payload['answer'].',,,,';
                            }else{
                                $response = array(
                                    'status'=>'ERROR',
                                    'message'=>'ERROR WHILE INSERTING DATA'
                                );
                                echo json_encode($response);
                            }
                            
                            $data = array(
                                'answer_id' => $payload['answer_id'],      
				    			'answer' => $answer,
				    			'exam_template_questions_id' => $etq->exam_template_questions_id,
				    			'application_id' => $payload['application_id'],
				    			'exam_id'=>2,
				    	    );
                            
                            $this->db->update('answer_bank', $data, array('answer_id' => $payload['answer_id']));
//                            $payload = array(
//                                'answer_id'=>$payload['answer_id'],             
//                                'answer' => $answer,
//                                'exam_template_questions_id' => $etq->exam_template_questions_id,
//                                'application_id' => $payload['application_id'],
//                                'exam_id' => 2
//                            );
                            
                            $response = array(
                                'status' => 'SUCCESS',
                                'message' => 'ANSWER SUBMITTED',
                                'payload' => $data
                            );
                            echo json_encode($response);
                        }
                        else{
                            $response = array(
                                    'status'=>'ERROR',
                                    'message'=>'NO QUESTION FOUND IN EXAM TEMPLATE QUESTIONS'
                            );
                            echo json_encode($response);
                        }
                    }
                    
                    else{
                        $response = array(
                                    'status'=>'ERROR',
                                    'message'=>'NO APPLICATION FOUND'
                        );
                        echo json_encode($response);
                    }
                }
                
                else{
                    $sql = $this->db->get_where("answer_bank", array("answer_id"=>$payload['answer_id']));
    			    $row = $sql->row();
                    $exp = explode(",", $row->answer);
                    $etq_id = $payload['questionnaire_id'] - 1;

    			    $this->db->where('questionnaire_id', $payload['questionnaire_id']);
                    $this->db->where('exam_id', 2);
                    $get = $this->db->get('exam_template_questions');
                    if($get->num_rows() > 0 ){
                        $etq = $get->row();
                        $this->db->where('questionnaire_id', $payload['questionnaire_id']);
                        $get1 = $this->db->get('questionnaire_bank');
                        $qb = $get1->row();

                        if($qb->questionnaire_answer_group == 'A'){
                            $ans = $exp[0]+$payload['answer'];
                            $answer = array(
                                'exam_template_questions_id' => $row->exam_template_questions_id.','.$etq_id,
                                'answer' => $ans.','.$exp[1].','.$exp[2].','.$exp[3].','.$exp[4]
                            );
                        }elseif($qb->questionnaire_answer_group == 'B'){
                            $ans = $exp[1]+$payload['answer'];
                            $answer = array(
                                'exam_template_questions_id' => $row->exam_template_questions_id.','.$etq_id,
                                'answer' => $exp[0].','.$ans.','.$exp[2].','.$exp[3].','.$exp[4]
                            );

                        }elseif($qb->questionnaire_answer_group == 'C'){
                            $ans = $exp[2]+$payload['answer'];
                            $answer = array(
                                'exam_template_questions_id' => $row->exam_template_questions_id.','.$etq_id,
                                'answer' => $exp[0].','.$exp[1].','.$ans.','.$exp[3].','.$exp[4]
                            );

                        }elseif($qb->questionnaire_answer_group == 'D'){
                            $ans = $exp[3]+$payload['answer'];
                            $answer = array(
                                'exam_template_questions_id' => $row->exam_template_questions_id.','.$etq_id,
                                'answer' => $exp[0].','.$exp[1].','.$exp[2].','.$ans.','.$exp[4]
                            );

                        }elseif($qb->questionnaire_answer_group == 'E'){
                            $ans = $exp[4]+$payload['answer'];
                            $answer = array(
                                'exam_template_questions_id' => $row->exam_template_questions_id.','.$etq_id,
                                'answer' => $exp[0].','.$exp[1].','.$exp[2].','.$exp[3].','.$ans
                            );
                        }elseif($qb->questionnaire_answer_group == 'G'){
                            $payload['answer'] = 0;
                            $ans = $exp[0]+$payload['answer'];
                            $answer = array(
                                'exam_template_questions_id' => $row->exam_template_questions_id.','.$etq_id,
                                'answer' => $ans.','.$exp[1].','.$exp[2].','.$exp[3].','.$exp[4]
                            );
                        }else{
                            $response = array(
                                'status'=>'ERROR',
                                'message'=>'ERROR WHILE INSERTING DATA'
                            );
                            echo json_encode($response);
                        }
                        
                        $this->db->update("answer_bank", $answer, array('answer_id' => $payload['answer_id']));
                        $this->db->where('answer_id', $payload['answer_id']);
                        $get = $this->db->get('answer_bank');
                        $row = $get->row();

                        $response = array(
                                    'status'=>'SUCCESS',
                                    'message'=>'SUCCESS',
                                    'payload'=>$row
                        );
                        echo json_encode($response);
                    }
                    
                    else{
                        $response = array(
                                    'status'=>'ERROR',
                                    'message'=>'NO QUESTION FOUND IN EXAM TEMPLATE QUESTIONS'
                        );
                        echo json_encode($response);
                    }
    		  }
    	   }
        }
    }
    
    public function countIQScore($payload){
        $sql = $this->db->get_where('answer_bank', array('answer_id'=>$payload['answer_id']));
        
        if($sql){
            foreach($sql->result() as $row){
                $application_id = $row->application_id;
                $answer =  $row->answer;
            }

            // $this->db->where('questionnaire_id', 2);
            // $sql = $this->db->get('questionnaire_bank');
            // $get = $sql->row();

            $explode = explode(",", $answer);
            $count = count($explode);

            // if($explode[0] == $get->questionnaire_answer){
            //     $count = count($explode);  
            // }
            // else{
            //     $count = count($explode) - 1;
            // }


            $data = array(
                'application_id' => $application_id,
                'questionnaire_result' => $count,
                'exam_id' => $row->exam_id
            );

            $this->db->where('exam_id', 1);
            $this->db->where('application_id', $application_id);
            $get = $this->db->get('exam_result');

            if($get->num_rows() > 0){
                $response = array(
                    'status' => 'FAILED',
                    'message' => 'EXAM RESULT ALREADY EXISTS'
                );

                echo json_encode($response);
            }
            else{
                $this->db->insert('exam_result', $data);
                $response = array(
                    'status' => 'SUCCESS',
                    'message' => 'SUCCESS',
                    'payload' => $data
                );

                echo json_encode($response);
            }
        }
        
        else{
            $this->db->insert('exam_result', $data);
            $response = array(
                'status' => 'ERROR',
                'message' => 'ANSWER ID DOES NOT EXIST',
                'payload' => $data
            );

            echo json_encode($response);
        }
        
        
    }
    
    public function countEQScore($payload){
    	$sql = $this->db->get_where("answer_bank", array("answer_id" =>$payload['answer_id']));
    	if($sql->num_rows() > 0){
    		$row = $sql->row();
    		$exp = explode(",", $row->answer);
    		$sum = array_sum($exp);
    		$quotient = $sum / 5;
    		$data = array(
    				'application_id'=>$row->application_id,
    				'questionnaire_result'=>$row->answer.','.$sum.','.$quotient,
    				'exam_id'=>$row->exam_id,
    		);
    		$insert = $this->db->insert("exam_result",$data);
    		$response = array(
    					'status'=>'SUCCESS',
    					'message'=>'SUCCESS INSERTING',
    					'payload'=>$data
    		);

    	}else{
    		$response = array(
    					'status'=>'ERROR',
    					'message'=>'NO ANSWER ID FOUND'
    		);
    		
    	}
    	echo json_encode($response);
    }
    
    public function getExamListSortById(){
    	$this->db->order_by("exam_id","asc");
    	$this->db->select("*");
    	$this->db->from("exam_template_questions etq");
    	$this->db->join("questionnaire_bank qb", "etq.questionnaire_id = qb.questionnaire_id");
    	$this->db->order_by("exam_id","asc");
    	$sql = $this->db->get();

    	if($sql){
    		foreach($sql->result() as $row){
    			$result[] = array(
                        'exam_id'=>$row->exam_id,
    					'questionnaire_id'=>$row->questionnaire_id,
                        'questionnaire_question'=>$row->questionnaire_question,
    					'questionnaire_question_pic'=>$row->questionnaire_question_pic,
    					'questionnaire_answer'=>$row->questionnaire_answer,
    					'questionnaire_choices'=>$row->questionnaire_choices
    					);
    		}
    	}else{
    		$response = array(
    					'status'=>'ERROR',
    					'message'=>'ERROR FETCHING EXAM LIST'
    		);
    	}
        
        $response = array(
    					'status'=>'SUCCESS',
    					'message'=>'SUCCESS FETCHING DATA',
                        'payload'=>$result
        );
    	return json_encode($response);
    }

    public function submitExamResult($payload){
        if($payload == null){
            $response = array(
                'status' => 'FAILED',
                'message' => 'PLEASE CHECK YOUR DATA'
            );
            echo json_encode($response);
        }
        elseif($payload != null){
            $iq = array(
                'application_id' => $payload['application_id'],
                'questionnaire_result' => $payload['iq_score'],
                'exam_id' => 1
            );
            $this->db->insert('exam_result', $iq);
            $iq_id = $this->db->insert_id();
            $sum = $payload['eqA']+$payload['eqB']+$payload['eqC']+$payload['eqD']+$payload['eqE'];
            $quo = $sum / 5;
            $eq = array(
                'application_id' => $payload['application_id'],
                'questionnaire_result' => $payload['eqA'].','.$payload['eqB'].','.$payload['eqC'].','.$payload['eqD'].','.$payload['eqE'].','.$sum.','.$quo,
                'exam_id' => 2
            );
            $this->db->insert('exam_result', $eq);
            $eq_id = $this->db->insert_id();

            $exam_result_ids = array(
                'iq_id' => $iq_id,
                'eq_id' => $eq_id
            );
            $response = array(
                'status' => 'SUCCESS',
                'message' => 'SUCCESS INSERTING DATA',
                'payload' => $exam_result_ids
            );
            echo json_encode($response);
        }
        else{
            $response = array(
                'status' => 'ERROR',
                'message' => 'ERROR'
            );
            echo json_encode($response);
        }
    }

    public function updateExamResult($payload){
        if($payload == null){
            $response = array(
                'status' => 'FAILED',
                'message' => 'PLEASE CHECK YOUR DATA'
            );
            echo json_encode($response);
        }
        elseif($payload != null){
            $this->db->where('exam_result_id', $payload['iq_id']);
            $get_iq = $this->db->get('exam_result');

            $this->db->where('exam_result_id', $payload['eq_id']);
            $get_eq = $this->db->get('exam_result');

            if($get_iq->num_rows() > 0 || $get_eq->num_rows() > 0){
                $iq_data = array(
                    'application_id' => $payload['application_id'],
                    'questionnaire_result' => $payload['iq_score']
                );
                $this->db->update('exam_result', $iq_data, array('exam_result_id' => $payload['iq_id']));

                $sum = $payload['eqA']+$payload['eqB']+$payload['eqC']+$payload['eqD']+$payload['eqE'];
                $quo = $sum / 5;
                $eq_data = array(
                    'application_id' => $payload['application_id'],
                    'questionnaire_result' => $payload['eqA'].','.$payload['eqB'].','.$payload['eqC'].','.$payload['eqD'].','.$payload['eqE'].','.$sum.','.$quo
                );
                $this->db->update('exam_result', $eq_data, array('exam_result_id' => $payload['eq_id']));

                $response = array(
                    'status' => 'SUCCESS',
                    'message' => 'SUCCESS UPDATING DATA'
                );
                echo json_encode($response);
            }

            else{
                $response = array(
                    'status' => 'FAILED',
                    'message' => 'DATA MAY NOT BE EXISTING'
                );
                echo json_encode($response);
            }
            
        }
        else{
            $response = array(
                'status' => 'ERROR',
                'message' => 'ERROR'
            );
            echo json_encode($response);
        }

    }
    
    public function getExamResult(){
        $this->db->select("*");
        $this->db->from("exam_result er");
        $this->db->join("application app", "app.application_id = er.application_id");
        $sql = $this->db->get();
        if($sql->num_rows() > 0){
            foreach($sql->result() as $row){   
               if($row->exam_id == 1){
                   $applicant = $this->db->get_where("applicant", array("applicant_id"=>$row->applicant_id));
                   foreach($applicant->result_array() as $row1){
                       $iq = array(
                           'application_id'=>$row->application_id,
                           'fname'=>$row1['applicant_fname'].' '.$row1['applicant_lname'],
                           'application_number'=>$row->application_number,
                           'IQ'=>$row->questionnaire_result,
                           'application_status'=>$row->application_status
                       );   
                   }
                   
               }
               elseif($row->exam_id == 2){
                   $exp = explode(",", $row->questionnaire_result);
                   $eq = array(
                       'A'=>$exp[0],
                       'B'=>$exp[1],
                       'C'=>$exp[2],
                       'D'=>$exp[3],
                       'E'=>$exp[4],
                       'SUM'=>$exp[5],
                       'QUO'=>$exp[6]
                   );
                   $data[] = array_merge($iq,$eq);
                   $response = array(
                               'status'=>'SUCCESS',
                               'message'=>'SUCCESS',
                               'payload'=>$data
                   );
               }
           }
           echo json_encode($response);
       }
        
       elseif($sql->num_rows() < 1){
           $response = array(
                       'status'=>'FAILED',
                       'message'=>'NO RECORD FOUND'
           );
           echo json_encode($response);
       }    
        
       else{
           $response = array(
                       'status'=>'ERROR',
                       'message'=>'ERROR'
           );
           echo json_encode($response);
       }
        
   }
    
    public function insertNewApplicant($payload){
        if($payload == null){
            $response = array('status' => 'FAILED',
                              'message' => 'PLEASE CHECK YOUR DATA');
            echo json_encode($response);
        }

        elseif($payload != null){

          $data = array(
                   'si_FNAME'=>$payload['si_FNAME'],
                   'si_MNAME'=>$payload['si_MNAME'],
                   'si_LNAME'=>$payload['si_LNAME'],
                   'si_BIRTHDATE'=>$payload['si_BIRTHDATE'],
                   'si_BIRTHPLACE'=>$payload['si_BIRTHPLACE'],
                   'si_GENDER'=>$payload['si_GENDER'],
                   'si_STREET'=>$payload['si_STREET'],
                   'si_BRGY'=>$payload['si_BRGY'],
                   'si_CITY'=>$payload['si_CITY'],
                   'si_DISTRICT'=>$payload['si_DISTRICT'],
                   'si_PROVINCE'=>$payload['si_PROVINCE'],
                   'si_EMAIL'=>$payload['si_EMAIL'],
                   'si_CONTACT'=>$payload['si_CONTACT'],
                   'student_type_ID'=>$payload['student_type_ID'],
                   'student_year_level_id'=>$payload['student_year_level_id'],
                   'student_admit_ID'=>$payload['student_admit_ID'],
                   'access_ID'=>$payload['access_ID'],
                   'added_by'=>$payload['added_by'],
                   'admitted_date'=>$payload['admitted_date'],
            );

            $check_contact = $payload['si_CONTACT'];
            $this->db->where('si_CONTACT', $check_contact);
            $check_email = $payload['si_EMAIL'];
            $this->db->where('si_EMAIL', $check_email);

            $query = $this->db2->get('student_information');
            $num = $query->num_rows();

            if($num == 1){
                $response = array('status' => 'FAILED',
                                  'message' => 'USER CONTACT NUMBER ALREADY EXISTS');
                echo json_encode($response);
            }
            
            elseif(!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $check_email)){
                $response = array('status' => 'FAILED',
                                  'message' => 'USER EMAIL IS INVALID');
                echo json_encode($response);
            }

            else{
                $insert = $this->db->insert("student_information",$data);

                if($insert){
                   $response = array(
                               'status'=>'SUCCESS',
                               'message'=>'SUCCESS INERTING DATA',
                               'payload'=>$data
                   );
                   
                }else{
                   $response = array(
                           'status'=>'ERROR',
                           'message'=>'ERROR IN INSERTING DATA'
                   );
                   
                }
            }
        }
        else{
            $response = array('status' => 'ERROR',
                              'message' => 'ERROR ENCOUNTERED');
            echo json_encode($response);
        }
   }
}
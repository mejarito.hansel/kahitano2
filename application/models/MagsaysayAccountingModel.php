<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");

class MagsaysayAccountingModel extends CI_Controller {
    
    function __construct(){
        header('Content-Type: application/json');
	    parent::__construct();	
        
        $CI = &get_instance();
        $this->db3 = $CI->load->database('third', TRUE);
    }

    public function insertFeeConfig($payload){
        if($payload == null){
            $response = array(
                'status' => 'FAILED',
                'message' => 'PLEASE CHECK YOUR DATA'
            );
            
            echo json_encode($response);
        }
        
        elseif($payload != null){        
            $count = count($payload);
            
            $this->db3->where('fee_type_id', $payload[0]['fee_type_id']);
            $check_type = $this->db3->get('Fee_type');
            
            $this->db3->where('fee_template_name', $payload[0]['fee_template_name']);
            $check_template = $this->db3->get('Fee_template');
            
            if($check_type->num_rows() < 1){             // to check if fee type exists
                $response = array(
                    'status' => 'ERROR',
                    'message' => 'FEE TYPE DOES NOT EXIST'
                );

                echo json_encode($response);
            }
                
            elseif($check_template->num_rows() > 0){
                $response = array(
                    'status' => 'ERROR',
                    'message' => 'FEE TEMPLATE ALREADY EXISTS'
                );

                echo json_encode($response);
            }
            
            elseif($check_template->num_rows() < 1){
                $insert = array('fee_template_name' => $payload[0]['fee_template_name']);
                $this->db3->insert('Fee_template', $insert);
                $fee_template_id = $this->db3->insert_id();
                
                for($i = 0; $i < $count; $i++){
                    $this->db3->where('fee_type_id', $payload[$i]['fee_type_id']);
                    $check_type = $this->db3->get('Fee_type');

                    

                    $result = array(
                        'fee_template_id' => $fee_template_id,
                        'fee_type_id' => $payload[$i]['fee_type_id'],
                        'fee_label' => $payload[$i]['fee_label'],
                        'fee_value' => $payload[$i]['fee_value']
                    );

                    $this->db3->insert('Fee_config', $result);
                    $response = array(
                        'status' => 'SUCCESS',
                        'message' => 'SUCCESS INSERTING DATA',
                        'payload' => $payload
                    );
                
                }
                
                echo json_encode($response);
            }
            
            else{
                $response = array(
                    'status' => 'ERROR',
                    'message' => 'ERROR'
                );
            
                echo json_encode($response);
            }
        }
        
        else{
            $response = array(
                    'status' => 'ERROR',
                    'message' => 'ERROR'
            );
            
            echo json_encode($response);
        }
    }
    
    public function getAllFeeConfig(){
        $sql = $this->db3->get('Fee_config'); //could be any table
		if ($sql) {
			if ($sql->num_rows() > 0) {
				$data = $sql->result();
				$response = array(
					'result' => 'SUCCESS',
					'message' => 'SUCCESS FETCHING DATA',
					'payload' => $data
				);
				return json_encode($response);
			}
			else {
				$response = array(
					'result' => 'FAILED',
					'message' => 'FAILED RETRIEVING DATA'
				);
				return json_encode($response);
			}
		}
		else {
			$response = array(
				'result' => 'ERROR',
				'message' => 'ERROR'
			);
			return json_encode($response);
			$error = $this->db3->error();
			print_r($error);
		}
    }
    
    public function getAllFeeConfigByTemplateID($payload){
        $this->db3->select('*');
        $this->db3->from('Fee_config fc');
        $this->db3->join('Fee_template ft', 'ft.fee_template_id = fc.fee_template_id');
        $this->db3->where('fc.fee_template_id', $payload['fee_template_id']);
        $sql = $this->db3->get(); //could be any table
		if ($sql) {
			if ($sql->num_rows() > 0) {
				$data = $sql->result();
				$response = array(
					'result' => 'SUCCESS',
					'message' => 'SUCCESS FETCHING DATA',
					'payload' => $data
				);
				echo json_encode($response);
			}
			else {
				$response = array(
					'result' => 'FAILED',
					'message' => 'FAILED RETRIEVING DATA'
				);
				echo json_encode($response);
			}
		}
		else {
			$response = array(
				'result' => 'ERROR',
				'message' => 'ERROR'
			);
			echo json_encode($response);
			$error = $this->db3->error();
			print_r($error);
		}
    }
    
    public function getAllFeeConfigByTypeID($payload){
        $this->db3->where('fee_type_id', $payload['fee_type_id']);
        $sql = $this->db3->get('Fee_config'); //could be any table
		if ($sql) {
			if ($sql->num_rows() > 0) {
				$data = $sql->result();
				$response = array(
					'result' => 'SUCCESS',
					'message' => 'SUCCESS FETCHING DATA',
					'payload' => $data
				);
				echo json_encode($response);
			}
			else {
				$response = array(
					'result' => 'FAILED',
					'message' => 'FAILED RETRIEVING DATA'
				);
				echo json_encode($response);
			}
		}
		else {
			$response = array(
				'result' => 'ERROR',
				'message' => 'ERROR'
			);
			echo json_encode($response);
			$error = $this->db3->error();
			print_r($error);
		}
    }
    
    public function getAllFeeConfigByStatus($payload){
        $this->db3->where('fee_config_status', $payload['fee_config_status']);
        $sql = $this->db3->get('Fee_config'); //could be any table
		if ($sql) {
			if ($sql->num_rows() > 0) {
				$data = $sql->result();
				$response = array(
					'result' => 'SUCCESS',
					'message' => 'SUCCESS FETCHING DATA',
					'payload' => $data
				);
				echo json_encode($response);
			}
			else {
				$response = array(
					'result' => 'FAILED',
					'message' => 'FAILED RETRIEVING DATA'
				);
				echo json_encode($response);
			}
		}
		else {
			$response = array(
				'result' => 'ERROR',
				'message' => 'ERROR'
			);
			echo json_encode($response);
			$error = $this->db3->error();
			print_r($error);
		}
    }
    
    public function updateFeeConfigRate($payload){
        if($payload == null){
            $response = array(
                'status' => 'FAILED',
                'message' => 'PLEASE CHECK YOUR DATA'
            );
            
            echo json_encode($response);
        }
        
        elseif($payload != null){
            $this->db3->where('fee_label', $payload['fee_label']);
            $this->db3->where('fee_template_id', $payload['fee_template_id']);
            $check = $this->db3->get('Fee_config');
            
            $data = $check->row();
            $num = $check->num_rows();
            if($num > 0){
                $data = $check->row();
                $result = array(
                    'fee_value' => $payload['fee_value']
                );
                
                $this->db3->update('Fee_config', $result, array('fee_label' => $payload['fee_label'], 'fee_template_id' => $payload['fee_template_id']));
                
                $this->db3->where('fee_label', $payload['fee_label']);
                $this->db3->where('fee_template_id', $payload['fee_template_id']);
                $check = $this->db3->get('Fee_config');
                $data = $check->row();
                
                $response = array(
                    'status' => 'SUCCESS',
                    'message' => 'SUCCESS UPDATING DATA',
                    'payload' => $data
                );
                
                echo json_encode($response);
            }
            
            else{
                $response = array(
                    'status' => 'ERROR',
                    'message' => 'DATA DOES NOT EXIST'
                );
            
                echo json_encode($response);
            }
        }
        
        else{
            $response = array(
                    'status' => 'ERROR',
                    'message' => 'ERROR'
            );
            
            echo json_encode($response);
        }
    }
    
    public function deleteFeeConfig($payload){
        if($payload == null){
            $response = array(
                'status' => 'FAILED',
                'message' => 'PLEASE CHECK YOUR DATA'
            );
            
            echo json_encode($response);
        }
        
        elseif($payload != null){
            $this->db3->where('fee_config_id', $payload['fee_config_id']);
            $check = $this->db3->get('Fee_config');
            
            $data = $check->row();
            $num = $check->num_rows();
            if($num > 0){
                
                $this->db3->delete('Fee_config', array('fee_config_id' => $payload['fee_config_id']));
                $response = array(
                    'status' => 'SUCCESS',
                    'message' => 'SUCCESS DELETING DATA'
                );
            
                echo json_encode($response);
            }
            
            else{
                $response = array(
                    'status' => 'ERROR',
                    'message' => 'DATA DOES NOT EXIST'
                );
            
                echo json_encode($response);
            }
        }
        
        else{
            $response = array(
                    'status' => 'ERROR',
                    'message' => 'ERROR'
            );
            
            echo json_encode($response);
        }
    }
    
// end of web services for fee config
    
//    public function insertFeeTemplate($payload){
//        if($payload == null){
//            $response = array(
//                'status' => 'FAILED',
//                'message' => 'PLEASE CHECK YOUR DATA'
//            );
//            
//            echo json_encode($response);
//        }
//        
//        elseif($payload != null){
//            $this->db3->where('acad_id', $payload['acad_id']);
//            $check = $this->db3->get('Fee_template');                  
//                
//            $num1 = $check->num_rows();      
//            
//            if($num1 > 0){                  //to check if the fee already exists
//                $response = array(
//                    'status' => 'ERROR',
//                    'message' => 'FEE TEMPLATE EXISTS'
//                );
//            
//                echo json_encode($response);
//            }
//            
//            else{
//                $result = array(
//                    'acad_id' => $payload['acad_id']
//                );
//                $this->db3->insert('Fee_template', $result);
//                
//                $response = array(
//                    'status' => 'SUCCESS',
//                    'message' => 'SUCCESS INSERTING DATA',
//                    'payload' => $result
//                );
//            
//                echo json_encode($response);
//            }
//        }
//        
//        else{
//            $response = array(
//                    'status' => 'ERROR',
//                    'message' => 'ERROR'
//            );
//            
//            echo json_encode($response);
//        }
//    }
    
    public function getAllFeeTemplate(){
        $sql = $this->db3->get('Fee_template'); //could be any table
		foreach($sql->result() as $row){
            $fee_template_id[] = $row->fee_template_id;
            
            $this->db3->select_sum('fee_value');
            $this->db3->where('fee_template_id', $row->fee_template_id);
            $get1 = $this->db3->get('Fee_config');
            
            $get1_row = $get1->row();
            $fee_value[] = $get1_row->fee_value;    
            
            $this->db3->where('fee_template_id', $row->fee_template_id);
            $get2 = $this->db3->get('Fee_template');
            
            $get2_row = $get2->row();
            $fee_template_name[] = $get2_row->fee_template_name;
        }
    
        $count = count($fee_template_id);
        
        for($i = 0; $i < $count; $i++){
            $result[] = array(
                'fee_template_id' => $fee_template_id[$i],
                'fee_value' => $fee_value[$i],
                'fee_template_name' => $fee_template_name[$i]
            );
        }
        
        
        $response = array(
            'status' => 'SUCCESS',
            'message' => 'SUCCESS FETCHING DATA',
            'payload' => $result
        );
            
        echo json_encode($response);
    }
    
    public function getAllFeeTemplateByID($payload){
        $this->db3->where('fee_template_id', $payload['fee_template_id']);
        $sql = $this->db3->get('Fee_template'); //could be any table
		if ($sql) {
			if ($sql->num_rows() > 0) {
				$data = $sql->result();
				$response = array(
					'result' => 'SUCCESS',
					'message' => 'SUCCESS FETCHING DATA',
					'payload' => $data
				);
				echo json_encode($response);
			}
			else {
				$response = array(
					'result' => 'FAILED',
					'message' => 'FAILED RETRIEVING DATA'
				);
				echo json_encode($response);
			}
		}
		else {
			$response = array(
				'result' => 'ERROR',
				'message' => 'ERROR'
			);
			echo json_encode($response);
			$error = $this->db3->error();
			print_r($error);
		}
    }
    
    public function getAllFeeTemplateByStatus($payload){
        $this->db3->where('fee_template_status', $payload['fee_template_status']);
        $sql = $this->db3->get('Fee_template'); //could be any table
		if ($sql) {
			if ($sql->num_rows() > 0) {
				$data = $sql->result();
				$response = array(
					'result' => 'SUCCESS',
					'message' => 'SUCCESS FETCHING DATA',
					'payload' => $data
				);
				echo json_encode($response);
			}
			else {
				$response = array(
					'result' => 'FAILED',
					'message' => 'FAILED RETRIEVING DATA'
				);
				echo json_encode($response);
			}
		}
		else {
			$response = array(
				'result' => 'ERROR',
				'message' => 'ERROR'
			);
			echo json_encode($response);
			$error = $this->db3->error();
			print_r($error);
		}
    }
    
    public function updateFeeTemplateStatusByID($payload){
        if($payload == null){
            $response = array(
                'status' => 'FAILED',
                'message' => 'PLEASE CHECK YOUR DATA'
            );
            
            echo json_encode($response);
        }
        elseif($payload != null){
            $this->db3->where('fee_template_id', $payload['fee_template_id']);
            $get = $this->db3->get('Fee_template');
            
            if($get->num_rows() < 1){
                $response = array(
                    'status' => 'FAILED',
                    'message' => 'FEE TEMPLATE DOES NOT EXIST'
                );

                echo json_encode($response);
            }
            elseif($get->num_rows() > 0){
                $data = array('fee_template_status' => $payload['fee_template_status']);
                $this->db3->update('Fee_template', $data, array('fee_template_id' => $payload['fee_template_id']));
            }
            
        }
        else{
            $response = array(
                'status' => 'ERROR',
                'message' => 'ERROR'
            );
            
            echo json_encode($response);
        }
    }
    
    public function deleteFeeTemplate($payload){
        if($payload == null){
            $response = array(
                'status' => 'FAILED',
                'message' => 'PLEASE CHECK YOUR DATA'
            );
            
            echo json_encode($response);
        }
        
        elseif($payload != null){
            $this->db3->where('fee_template_id', $payload['fee_template_id']);
            $check = $this->db3->get('Fee_template');
            
            $data = $check->row();
            $num = $check->num_rows();
            if($num > 0){
                
                $this->db3->delete('Fee_template', array('fee_template_id' => $payload['fee_template_id']));
                $response = array(
                    'status' => 'SUCCESS',
                    'message' => 'SUCCESS DELETING DATA'
                );
            
                echo json_encode($response);
            }
            
            else{
                $response = array(
                    'status' => 'ERROR',
                    'message' => 'DATA DOES NOT EXIST'
                );
            
                echo json_encode($response);
            }
        }
        
        else{
            $response = array(
                    'status' => 'ERROR',
                    'message' => 'ERROR'
            );
            
            echo json_encode($response);
        }
    }

// end of web services for fee template
    
    public function insertFeeType($payload){
        if($payload == null){
            $response = array(
                'status' => 'FAILED',
                'message' => 'PLEASE CHECK YOUR DATA'
            );
            
            echo json_encode($response);
        }
        
        elseif($payload != null){
            $this->db3->where('fee_type_name', $payload['fee_type_name']);      
            $check = $this->db3->get('Fee_type');
                
            $num1 = $check->num_rows(); 
            
            if($num1 > 0){                  //to check if the fee already exists
                $response = array(
                    'status' => 'ERROR',
                    'message' => 'FEE ALREADY EXISTS'
                );
            
                echo json_encode($response);
            }
            
            else{
                $result = array(
                    'fee_type_name' => $payload['fee_type_name']
                );
                $this->db3->insert('Fee_type', $result);
                
                $response = array(
                    'status' => 'SUCCESS',
                    'message' => 'SUCCESS INSERTING DATA',
                    'payload' => $result
                );
            
                echo json_encode($response);
            }
        }
        
        else{
            $response = array(
                    'status' => 'ERROR',
                    'message' => 'ERROR'
            );
            
            echo json_encode($response);
        }
        
    }
    
    public function getAllFeeType(){
        $sql = $this->db3->get('Fee_type'); //could be any table
		if ($sql) {
			if ($sql->num_rows() > 0) {
				$data = $sql->result();
				$response = array(
					'result' => 'SUCCESS',
					'message' => 'SUCCESS FETCHING DATA',
					'payload' => $data
				);
				return json_encode($response);
			}
			else {
				$response = array(
					'result' => 'FAILED',
					'message' => 'FAILED RETRIEVING DATA'
				);
				return json_encode($response);
			}
		}
		else {
			$response = array(
				'result' => 'ERROR',
				'message' => 'ERROR'
			);
			return json_encode($response);
			$error = $this->db3->error();
			print_r($error);
		}
    }
    
    public function getAllFeeTypeByID($payload){
        $this->db3->where('fee_type_id', $payload['fee_type_id']);
        $sql = $this->db3->get('Fee_type'); //could be any table
		if ($sql) {
			if ($sql->num_rows() > 0) {
				$data = $sql->result();
				$response = array(
					'result' => 'SUCCESS',
					'message' => 'SUCCESS FETCHING DATA',
					'payload' => $data
				);
				echo json_encode($response);
			}
			else {
				$response = array(
					'result' => 'FAILED',
					'message' => 'FAILED RETRIEVING DATA'
				);
				echo json_encode($response);
			}
		}
		else {
			$response = array(
				'result' => 'ERROR',
				'message' => 'ERROR'
			);
			echo json_encode($response);
			$error = $this->db3->error();
			print_r($error);
		}
    }
    
    public function getAllFeeTypeByStatus($payload){
        $this->db3->where('fee_type_status', $payload['fee_type_status']);
        $sql = $this->db3->get('Fee_type'); //could be any table
		if ($sql) {
			if ($sql->num_rows() > 0) {
				$data = $sql->result();
				$response = array(
					'result' => 'SUCCESS',
					'message' => 'SUCCESS FETCHING DATA',
					'payload' => $data
				);
				echo json_encode($response);
			}
			else {
				$response = array(
					'result' => 'FAILED',
					'message' => 'FAILED RETRIEVING DATA'
				);
				echo json_encode($response);
			}
		}
		else {
			$response = array(
				'result' => 'ERROR',
				'message' => 'ERROR'
			);
			echo json_encode($response);
			$error = $this->db3->error();
			print_r($error);
		}
    }
    
    public function updateFeeTypeName($payload){
        if($payload == null){
            $response = array(
                'status' => 'FAILED',
                'message' => 'PLEASE CHECK YOUR DATA'
            );
            
            echo json_encode($response);
        }
        
        elseif($payload != null){
            $this->db3->where('fee_type_id', $payload['fee_type_id']);
            $check = $this->db3->get('Fee_type');
            
            $data = $check->row();
            $num = $check->num_rows();
            if($num > 0){
                $data = $check->row();
                $result = array(
                    'fee_type_name' => $payload['fee_type_name']
                );
                
                $this->db3->update('Fee_type', $result, array('fee_type_id' => $payload['fee_type_id']));
                $response = array(
                    'status' => 'SUCCESS',
                    'message' => 'SUCCESS UPDATING DATA'
                );
            
                echo json_encode($response);
            }
            
            else{
                $response = array(
                    'status' => 'ERROR',
                    'message' => 'DATA DOES NOT EXIST'
                );
            
                echo json_encode($response);
            }
        }
        
        else{
            $response = array(
                    'status' => 'ERROR',
                    'message' => 'ERROR'
            );
            
            echo json_encode($response);
        }
    }
    
    public function deleteFeeType($payload){
        if($payload == null){
            $response = array(
                'status' => 'FAILED',
                'message' => 'PLEASE CHECK YOUR DATA'
            );
            
            echo json_encode($response);
        }
        
        elseif($payload != null){
            $this->db3->where('fee_type_id', $payload['fee_type_id']);
            $check = $this->db3->get('Fee_type');
            
            $data = $check->row();
            $num = $check->num_rows();
            if($num > 0){
                
                $this->db3->delete('Fee_type', array('fee_type_id' => $payload['fee_type_id']));
                $response = array(
                    'status' => 'SUCCESS',
                    'message' => 'SUCCESS DELETING DATA'
                );
            
                echo json_encode($response);
            }
            
            else{
                $response = array(
                    'status' => 'ERROR',
                    'message' => 'DATA DOES NOT EXIST'
                );
            
                echo json_encode($response);
            }
        }
        
        else{
            $response = array(
                    'status' => 'ERROR',
                    'message' => 'ERROR'
            );
            
            echo json_encode($response);
        }
    }
}

?>
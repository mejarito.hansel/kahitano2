<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");

class V1_model extends CI_Model
{	
    function __construct(){
        header('Content-Type: application/json');
		parent::__construct();
    }
        
	public function getApplicationByApplicantID($payload){ //application 
        $this->db->where('applicant_id', $payload['applicant_id']);
        $query = $this->db->get('application');
           
        $result = $query->result();
            
        if($query->num_rows() > 0){
           $response = array('status' => 'SUCCESS',
                             'message' => 'SUCCESS FETCHING DATA',
                             'payload' => $result); // if success
               
            echo json_encode($response);
        }
        else{
           $response = array('status' => 'FAILED',
                             'message' => 'FAILED FETCHING DATA'); // if failed
           echo json_encode($response);
        }
    }
    
    public function getApplicationByAcadIDandApplicantID($payload){
        $this->db->where('acad_id', $payload['acad_id']);
        $this->db->where('applicant_id', $payload['applicant_id']);
        $query = $this->db->get('application');
           
        $result = $query->result();
            
        if($query->num_rows() > 0){
           $response = array('status' => 'SUCCESS',
                             'message' => 'SUCCESS FETCHING DATA',
                             'payload' => $result); // if success
               
            echo json_encode($response);
        }
        else{
           $response = array('status' => 'FAILED',
                             'message' => 'FAILED FETCHING DATA'); // if failed
           echo json_encode($response);
        }
    }
		
	public function getApplicationByStatus($payload){
    	$this->db->select('*');
    	$this->db->from("applicant ap");
    	$this->db->join("application app", "ap.applicant_id = app.applicant_id","left");
    	$this->db->where("app.application_status",$payload['application_status']);
    	$sql = $this->db->get();
    	if($sql){
    		$response = array(
    					'status'=>'SUCCESS',
    					'message'=>'SUCCESS',
    					'payload'=>$sql->result()
    		);
    	}else{
    		$response = array(
    					'status'=>'ERROR',
    					'message'=>'ERROR'
    		);
    	}
    	echo json_encode($response);
    }
 
    public function getQuestionnaireList(){ //questionnaire_bank
        $sql = $this->db->get('questionnaire_bank'); //could be any table
        if($sql){
           if($sql->num_rows()>0) {
               $data = $sql->result();
               $response = array('status' => 'SUCCESS',
                                 'message' => 'SUCCESS FETCHING DATA',
                                 'payload' => $data);
                  
                return json_encode($response);
            }
            else{
               $response = array('status' => 'FAILED',
                                 'message' => 'FAILED RETRIEVING DATA');
               return json_encode($response);
            }        
        }
        else{
           $response = array('status' => 'ERROR',
                             'message' => 'ERROR');
           return json_encode($response);            
           $error = $this->db->error();
           print_r($error);
        }
    }		
		
	public function getQuestionnaireByID($payload){ //questionnaire_bank 
        $this->db->where('questionnaire_id', $payload['questionnaire_id']);
        $query = $this->db->get('questionnaire_bank');
          
        $result = $query->row();
            
        if($query->num_rows() == 1){
           $response = array('status' => 'SUCCESS',
                             'message' => 'SUCCESS FETCHING DATA',
                              'payload' => $result); // if success
               
           echo json_encode($response);
        }
        else{
           $response = array('status' => 'FAILED',
                             'message' => 'FAILED FETCHING DATA'); // if failed
           echo json_encode($response);
        }
    }

    public function getExamTemplateList(){ //exam_template
		$sql = $this->db->get('exam_template'); //could be any table
       	if($sql){
           if($sql->num_rows()>0) {
               $data = $sql->result();
               $response = array('status' => 'SUCCESS',
                                 'message' => 'SUCCESS FETCHING DATA',
                                 'payload' => $data);
                  
               return json_encode($response);
            }
            else{
                $response = array('status' => 'FAILED',
                                 'message' => 'FAILED RETRIEVING DATA');
                return json_encode($response);
            }        
        }
        else{
            $response = array('status' => 'ERROR',
                              'message' => 'ERROR');
            return json_encode($response);            
            $error = $this->db->error();
            print_r($error);
        }
    }		
		
    public function getExamTemplateByID($payload){ //exam_template 
        $this->db->where('exam_id', $payload['exam_id']);
        $query = $this->db->get('exam_template');
           
        $result = $query->row();
            
        if($query->num_rows() == 1){
            $response = array('status' => 'SUCCESS',
                              'message' => 'SUCCESS FETCHING DATA',
                              'payload' => $result); // if success
               
            echo json_encode($response);
        }
        else{
            $response = array('status' => 'FAILED',
                              'message' => 'FAILED FETCHING DATA'); // if failed
            echo json_encode($response);
        }
    }

	public function getAcademicSemList(){ //exam_template
        $sql = $this->db->get('academic_sem'); //could be any table
        if($sql){
           if($sql->num_rows()>0) {
               $data = $sql->result();
               $response = array('status' => 'SUCCESS',
                                 'message' => 'SUCCESS FETCHING DATA',
                                 'payload' => $data);
                  
               return json_encode($response);
           }
           else{
                $response = array('status' => 'FAILED',
                                 'message' => 'FAILED RETRIEVING DATA');
                return json_encode($response);
            }        
        }
        else{
            $response = array('status' => 'ERROR',
                              'message' => 'ERROR');
            return json_encode($response);            
            $error = $this->db->error();
            print_r($error);
        }
    }		
		
    public function getAcademicSemByStatus($payload){ //academic_sem 
        $this->db->where('acad_status', $payload['acad_status']);
        $query = $this->db->get('academic_sem');
           
        $result = $query->result();
            
        if($query->num_rows() > 0){
            $response = array('status' => 'SUCCESS',
                              'message' => 'SUCCESS FETCHING DATA',
                              'payload' => $result); // if success
               
        }
        else{
            $response = array('status' => 'FAILED',
                              'message' => 'FAILED RETRIEVING DATA'); // if failed  
         	}
        echo json_encode($response);
    }
        
    public function getAcademicSemByYearAndSem($payload){
        $this->db->where('acad_year', $payload['acad_year']);
        $this->db->where('acad_sem', $payload['acad_sem']);
        $query = $this->db->get('academic_sem');
           
        if($query->num_rows() > 0){
            $result = $query->result();
            $response = array('status' => 'SUCCESS',
                              'message' => 'SUCCESS FETCHING DATA',
                              'payload' => $result); // if success
               
        }
        else{
            $response = array('status' => 'FAILED',
                              'message' => 'FAILED RETRIEVING DATA'); // if failed  
         	}
        echo json_encode($response);
    }

}

?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");

class Ams_model extends CI_model{
    
    function __construct(){
        header('Content-Type: application/json');
		parent::__construct();
        
        $CI = &get_instance();
        $this->db2 = $CI->load->database('second', TRUE);
    }
    
// ================ WEB SERVICES FOR DEPARTMENT AND STUDENT
    
    public function fetchAllDepartmentList(){
        $sql = $this->db2->get('department_list'); //could be any table
           if($sql){
              if($sql->num_rows()>0) {
                  $data = $sql->result();
                  $response = array('status' => 'SUCCESS',
                                     'message' => 'SUCCESS FETCHING DATA',
                                     'payload' => $data);
                 
                  echo json_encode($response);
               }
              else{
                  $response = array('status' => 'FAILED',
                                     'message' => 'FAILED RETRIEVING DATA');
                   echo json_encode($response);
              }        
           }
           else{
              $response = array('status' => 'ERROR',
                                'message' => 'ERROR');
               echo json_encode($response);            
               $error = $this->db2->error();
               print_r($error);
           }
   }
   
    public function fetchDepartmentListByID($payload){ //department_list
     $this->db2->where('department_ID', $payload['department_ID']);
           $query = $this->db2->get('department_list');
         
           $result = $query->row();
           
           if($query->num_rows() == 1){
              $response = array('status' => 'SUCCESS',
                                'message' => 'SUCCESS FETCHING DATA',
                                 'payload' => $result); // if success
             
              echo json_encode($response);
           }
           else{
              $response = array('status' => 'FAILED',
                                'message' => 'FAILED FETCHING DATA'); // if failed
              echo json_encode($response);
          }
   }
   
    public function fetchStudentInfoByID($payload){ //department_list
     $this->db2->where('student_ID', $payload['student_ID']);
           $query = $this->db2->get('student_information');
         
           $result = $query->row();
           
           if($query->num_rows() == 1){
              $response = array('status' => 'SUCCESS',
                                'message' => 'SUCCESS FETCHING DATA',
                                 'payload' => $result); // if success
             
              echo json_encode($response);
           }
           else{
              $response = array('status' => 'FAILED',
                                'message' => 'FAILED FETCHING DATA'); // if failed
              echo json_encode($response);
          }
   }
    
// ================ WEB SERVICES FOR ACCOUNT INFO AND ACCOUNT LEVEL
    
    public function fetchAllAccount(){
        // $sql = $this->db2->get('account_info');
        $sql = $this->db2->get("account_info ai");
        $this->db2->select("*");
        $this->db2->from("account_info ai");
        $this->db2->join("account_level al", "ai.access_ID = al.access_ID");
        $sql = $this->db2->get(); //could be any table
        if($sql){
          if($sql->num_rows()>0) {
              $data = $sql->result();
              $response = array('status' => 'SUCCESS',
                                'message' => 'SUCCESS FETCHING DATA',
                                'payload' => $data);
                
              echo json_encode($response);
           }
           else{
              $response = array('status' => 'FAILED',
                                'message' => 'FAILED RETRIEVING DATA');
              echo json_encode($response);
           }        
        }
        else{
            $response = array('status' => 'ERROR',
                              'message' => 'ERROR');
            echo json_encode($response);            
            $error = $this->db2->error();
            print_r($error);
        }
    }

    
    public function fetchAllUI(){
        $sql = $this->db2->get('user_instructor'); //could be any table
        if($sql){
          if($sql->num_rows()>0) {
              $data = $sql->result();
              $response = array('status' => 'SUCCESS',
                                'message' => 'SUCCESS FETCHING DATA',
                                'payload' => $data);
                
              echo json_encode($response);
           }
           else{
              $response = array('status' => 'FAILED',
                                'message' => 'FAILED RETRIEVING DATA');
              echo json_encode($response);
           }        
        }
        else{
            $response = array('status' => 'ERROR',
                              'message' => 'ERROR');
            echo json_encode($response);            
            $error = $this->db2->error();
            print_r($error);
        }
    }
    
    public function fetchUIByAccountID($payload){
        if($payload == null){
            $response = array(
                'status' => 'FAILED',
                'message' => 'PLEASE CHECK YOUR DATA'
            );
            echo json_encode($response);   
        }
        elseif($payload != null){
            $this->db2->where('account_ID', $payload['account_ID']);
            $get = $this->db2->get('user_instructor');
            
            $row = $get->row();
            if($get->num_rows() == 1){
                $response = array(
                    'status' => 'SUCCESS',
                    'message' => 'SUCCESS FETCHING DATA',
                    'payload' => $row
                );
                echo json_encode($response);   
            }
            else{
                $response = array(
                    'status' => 'ERROR',
                    'message' => 'DATA DOES NOT EXIST'
                );
                echo json_encode($response); 
            }
        }
        else{
            $response = array(
                'status' => 'ERROR',
                'message' => 'ERROR'
            );
            echo json_encode($response);
        }
        
    }
    
    public function fetchAllAccountLevel(){
		$sql = $this->db2->get("account_level");
		if($sql){
			$response = array(
						'status'=>'SUCCESS',
						'message'=>'SUCCESS FETCHING',
						'payload'=>$sql->result()
			);
		}else{
			$response = array(
						'status'=>'ERROR',
						'message'=>'ERROR IN SQL'
			);
		}
		echo json_encode($response);
	}
    
    public function fetchAccountByAccountID($payload){
        if($payload == null){
            $response = array(
                'status' => 'FAILED',
                'message' => 'PLEASE CHECK YOUR DATA'
            );
            echo json_encode($response);   
        }
        elseif($payload != null){
            $this->db2->where('account_ID', $payload['account_ID']);
            $get = $this->db2->get('account_info');
            
            if($get->num_rows() > 0){
                $row = $get->row();
                $response = array(
                    'status' => 'SUCCESS',
                    'message' => 'SUCCESS FETCHING DATA',
                    'payload' => $row
                );
                echo json_encode($response);   
            }
            else{
                $response = array(
                    'status' => 'ERROR',
                    'message' => 'DATA DOES NOT EXIST'
                );
                echo json_encode($response); 
            }
        }
        else{
            $response = array(
                'status' => 'ERROR',
                'message' => 'ERROR'
            );
            echo json_encode($response);
        }
        
    }
    
    public function createAccountInfo($payload){
        if($payload == null){
            $response = array(
                'status' => 'FAILED',
                'message' => 'PLEASE CHECK YOUR DATA'
            );
            echo json_encode($response);   
        }
        elseif($payload != null){
            $this->db2->where('account_UNAME', $payload['account_UNAME']);
            $get1 = $this->db2->get('account_info');
            
            $this->db2->where('department_ID', $payload['department_ID']);
            $get2 = $this->db2->get('department_list');
            
            $this->db2->where('access_ID', $payload['access_ID']);
            $get3 = $this->db2->get('account_level');
            
            if($get1->num_rows() > 0){
                $response = array(
                    'status' => 'ERROR',
                    'message' => 'USERNAME ALREADY EXISTS'
                );
                echo json_encode($response);   
            }
            elseif($get2->num_rows() < 1){
                $response = array(
                    'status' => 'ERROR',
                    'message' => 'DEPARTMENT DOES NOT EXIST'
                );
                echo json_encode($response);  
            }
            elseif($get3->num_rows() < 1){
                $response = array(
                    'status' => 'ERROR',
                    'message' => 'ACCESS NAME DOES NOT EXIST'
                );
                echo json_encode($response); 
            }
            else{
                $data = array(
                    'access_ID' => $payload['access_ID'],
                    'department_ID' => $payload['department_ID'],
                    'employee_NO' => $payload['employee_NO'],
                    'RFID' => $payload['RFID'],
                    'account_UNAME' => $payload['account_UNAME'],
                    'account_PASS' => base64_encode('magsaysay'),
                    'account_FNAME' => $payload['account_FNAME'],
                    'account_MNAME' => $payload['account_MNAME'],
                    'account_LNAME' => $payload['account_LNAME'],
                    'account_CHANGEPASS' => 1
                );

                $this->db2->insert('account_info', $data);
                $id = $this->db2->insert_id();
                
                $this->db2->where('account_ID', $id);
                $get = $this->db2->get('account_info');
                $result = $get->row();
                $response = array(
                    'status' => 'SUCCESS',
                    'message' => 'SUCCESS INSERTING DATA',
                    'payload' => $result
                );
                echo json_encode($response);
                if ($payload['access_ID'] == 8) {

                    $inst_data = array(
                        // 'instructor_NAME' => $payload['account_FNAME'].' '.$payload['account_MNAME'].' '.$payload['account_LNAME'],
                        'instructor_NAME' => $payload['account_FNAME'],
                        'instructor_MNAME' => $payload['account_MNAME'],
                        'instructor_LNAME' => $payload['account_LNAME']
                    );
                    
                    $this->db2->insert('instructor', $inst_data);

                    $ui = $this->db2->insert_id();

                    $data1 = array(
                        'account_ID' => $id,
                        'instructor_ID' => $ui
                    );
                    $this->db2->insert('user_instructor', $data1);
                }
            }
        }
        else{
            $response = array(
                'status' => 'ERROR',
                'message' => 'ERROR'
            );
            echo json_encode($response);   
        }
    }
    
    public function updateAccountInfo($payload){
        // error_reporting(0);
        if($payload == null){
            $response = array(
                'status' => 'FAILED',
                'message' => 'PLEASE CHECK YOUR DATA'
            );
            echo json_encode($response);
        }
        elseif($payload != null){
            $this->db2->where('account_ID !=', $payload['account_ID']);
            $this->db2->where('account_UNAME', $payload['account_UNAME']);
            $get = $this->db2->get('account_info');
            
            if($get->num_rows() > 0){
                $response = array(
                    'status' => 'ERROR',
                    'message' => 'USERNAME ALREADY EXISTS'
                );
                echo json_encode($response);
            }
            else{
                $this->db2->where('account_ID', $payload['account_ID']);
                $get = $this->db2->get('account_info');
                
                if($payload['account_PASS'] == null){
                    $row = $get->row();
                    $data = array(
                        'account_FNAME' => $payload['account_FNAME'],
                        'account_MNAME' => $payload['account_MNAME'],
                        'account_LNAME' => $payload['account_LNAME'],
                        'account_UNAME' => $payload['account_UNAME'],
                        'account_PASS' => $row->account_PASS,
                        'department_ID' => $payload['department_ID'],
                        'employee_NO' => $payload['employee_NO'],
                        'access_ID' => $payload['access_ID'],
                        'account_ACTIVE' => $payload['account_ACTIVE']
                    );
                }
                else{
                    $data = array(
                        'account_FNAME' => $payload['account_FNAME'],
                        'account_MNAME' => $payload['account_MNAME'],
                        'account_LNAME' => $payload['account_LNAME'],
                        'account_UNAME' => $payload['account_UNAME'],
                        'account_PASS' => base64_encode($payload['account_PASS']),
                        'department_ID' => $payload['department_ID'],
                        'employee_NO' => $payload['employee_NO'],
                        'access_ID' => $payload['access_ID'],
                        'account_ACTIVE' => $payload['account_ACTIVE']
                    );
                }
                
                if($payload['access_ID'] == 8) {

                    $inst_data = array(
                        // 'instructor_NAME' => $payload['account_FNAME'].' '.$payload['account_MNAME'].' '.$payload['account_LNAME'],
                        'instructor_NAME' => $payload['account_FNAME'],
                        'instructor_MNAME' => $payload['account_MNAME'],
                        'instructor_LNAME' => $payload['account_LNAME']
                    );
                    //check if user exist start
                    $this->db2->where('account_ID', $payload['account_ID']);
                    $sql_checkuser = $this->db2->get('user_instructor');
                    $result_user = $sql_checkuser->result_array();
                    $count = count($result_user);
                    
                    if(($count == null) ||($count == '0')){
                      //check if user exist end
                    $this->db2->insert('instructor', $inst_data);

                    $ui = $this->db2->insert_id();

                    $data1 = array(
                        'account_ID' => $payload['account_ID'],
                        'instructor_ID' => $ui
                    );
                    $this->db2->insert('user_instructor', $data1);

                    }else{

                        $this->db2->set('status', '1');
                        $this->db2->where('account_ID',$payload['account_ID']);
                        $this->db2->update('user_instructor');

                        $this->db2->select('instructor_ID');
                        $this->db2->where('account_ID',$payload['account_ID']);
                        $hehe = $this->db2->get('user_instructor');
                        $result1 = $hehe->row();

                        $this->db2->set('status', '1');
                        $this->db2->where('instructor_ID',$result1->instructor_ID);
                        $this->db2->update('instructor');

                    }

                }else{

                        $this->db2->select('instructor_ID');
                        $this->db2->where('account_ID',$payload['account_ID']);
                        $hehe = $this->db2->get('user_instructor');
                        $result1 = $hehe->row();

                        $this->db2->set('status', '2');
                        $this->db2->where('instructor_ID',$result1->instructor_ID);
                        $this->db2->update('instructor');

                        $this->db2->set('status', '2');
                        $this->db2->where('account_ID',$payload['account_ID']);
                        $this->db2->update('user_instructor');
                       
                   
                }

                $this->db2->update('account_info', $data, array('account_ID' => $payload['account_ID']));

                $response = array(
                    'status' => 'SUCCESS',
                    'message' => 'SUCCESS UPDATING DATA',
                    'payload' => $payload
                );
                echo json_encode($response);    
            }
        }
        else{
            $response = array(
                'status' => 'ERROR',
                'message' => 'ERROR'
            );
            echo json_encode($response);
        }
    }
    
    public function updatePassword($payload){
        if($payload == null){
            $response = array(
                'status' => 'FAILED',
                'message' => 'PLEASE CHECK YOUR DATA'
            );
            echo json_encode($response);
        }
        elseif($payload != null){
            $this->db2->where('account_ID', $payload['account_ID']);
            $get = $this->db2->get('account_info');
            
            if($get->num_rows() > 0){
                $data = array(
                    'account_PASS' => base64_encode($payload['account_PASS'])
                );
                $this->db2->update('account_info', $data, array('account_ID' => $payload['account_ID']));
                
                $response = array(
                    'status' => 'SUCCESS',
                    'message' => 'SUCCESS UPDATING PASSWORD'
                );
                echo json_encode($response);
            }
            else{
                $response = array(
                    'status' => 'ERROR',
                    'message' => 'ACCOUNT DOES NOT EXIST'
                );
                echo json_encode($response);
            }
        }
        else{
            $response = array(
                'status' => 'ERROR',
                'message' => 'ERROR'
            );
            echo json_encode($response);
        }
    }

    public function updateStudentPassword($payload){
        if($payload == null){
            $response = array(
                'status' => 'FAILED',
                'message' => 'PLEASE CHECK YOUR DATA'
            );
            echo json_encode($response);
        }
        elseif($payload != null){
            $this->db2->where('si_ID', $payload['si_ID']);
            $get = $this->db2->get('student_information');
            
            if($get->num_rows() > 0){
                $data = array(
                    'password' => base64_encode($payload['password'])
                );
                $this->db2->update('student_information', $data, array('si_ID' => $payload['si_ID']));
                
                $response = array(
                    'status' => 'SUCCESS',
                    'message' => 'SUCCESS UPDATING PASSWORD'
                );
                echo json_encode($response);
            }
            else{
                $response = array(
                    'status' => 'ERROR',
                    'message' => 'ACCOUNT DOES NOT EXIST'
                );
                echo json_encode($response);
            }
        }
        else{
            $response = array(
                'status' => 'ERROR',
                'message' => 'ERROR'
            );
            echo json_encode($response);
        }
    }
    
// ================ WEB SERVICES FOR CLEARANCE AND SEM
    
    public function createClearance($payload){
        $date = date('Y-m-d H:i:s');
        $data = array(
                'si_ID'=>$payload['si_ID'],
                'sem_ID'=>$payload['sem_ID'],
                'department_ID'=>$payload['department_ID'],
                'blocked_by'=>$payload['account_ID'],
                'blocked_date'=>$date,
                'blocked_remakrs'=>$payload['blocked_remakrs'],
                'clearance_STAT'=>1
        );
        $sql = $this->db2->insert("clearance", $data);
        if($sql){
            $response = array(
                        'status'=>'SUCCESS',
                        'message'=>'SUCCESS BLOCKING',
                        'payload'=>$data
            );
        }else{
            $response = array(
                        'status'=>'ERROR',
                        'message'=>'ERROR BLOCKING'
            );
        }
        echo json_encode($response);
    }
    
    public function updateClearance($payload){
        $date = date('Y-m-d H:i:s');
        $this->db2->set('unblocked_date',$date);
        $this->db2->set('clearance_STAT', 2);
        $this->db2->set('unblocked_by', $payload['account_ID']);
        $this->db2->set('unblocked_remarks', $payload['unblocked_remarks']);
        $this->db2->where('clearance_ID', $payload['clearance_ID']);
        $sql = $this->db2->update('clearance');
        if($sql){
            $response = array(
                        'status'=>'SUCCESS',
                        'message'=>'SUCCESS BLOCKING',
                        'payload'=>$payload
            );
        }else{
            $response = array(
                        'status'=>'ERROR',
                        'message'=>'ERROR BLOCKING'
            );
        }
        echo json_encode($response);
    }
    
    public function fetchAllClearance(){
        $this->db2->select("*");
        $this->db2->from("clearance a");
        $this->db2->join("student_information b", "a.si_ID = b.si_ID", "left");
        $this->db2->join("semester c", "a.sem_ID = c.sem_ID", "left");
        $this->db2->join("account_info d", "a.blocked_by = d.account_ID AND a.unblocked_by = d.account_ID", "left");
        $this->db2->join("department_list e", "a.department_ID = e.department_ID", "left");
        $this->db2->or_where("clearance_STAT ",1);
        $sql = $this->db2->get();
        if($sql){
            $response = array(
                        'status'=>'SUCCESS',
                        'message'=>'SUCCESS FETCHING',
                        'payload'=>$sql->result()
            );
        }else{
            $response = array(
                        'status'=>'ERROR',
                        'message'=>'ERROR FETCHING'
            );
        }
        echo json_encode($response);
    }
    
    public function fetchAllClearanceById($payload){
        $this->db2->select("*");
        $this->db2->from("clearance a");
        $this->db2->join("student_information b", "a.si_ID = b.si_ID", "left");
        $this->db2->join("semester c", "a.sem_ID = c.sem_ID", "left");
        $this->db2->join("account_info d", "a.blocked_by = d.account_ID", "left");
        $this->db2->join("department_list e", "a.department_ID = e.department_ID", "left");
        $this->db2->where("clearance_ID",$payload['clearance_ID']);
        $sql = $this->db2->get();
        if($sql){
            $result = $sql->row();
            if($result->clearance_STAT == 3){
                $response = array(
                            'status'=>'ERROR',
                            'message'=>'SOFT DELETED'
                );
            }else{
                $response = array(
                        'status'=>'SUCCESS',
                        'message'=>'SUCCESS FETCHING',
                        'payload'=>$sql->result()
                );  
            }
            
        }else{
            $response = array(
                        'status'=>'ERROR',
                        'message'=>'ERROR FETCHING'
            );
        }
        echo json_encode($response);
    }
    
    public function fetchAllSem(){
        $this->db2->order_by("sem_ID","asc");
        $sql = $this->db2->get("semester");
        if($sql){
            $response = array(
                        'status'=>'SUCCESS',
                        'message'=>'SUCCESS FETCHING',
                        'payload'=>$sql->result()
            );
        }else{
            $response = array(
                        'status'=>'ERROR',
                        'message'=>'ERROR FETCHING'
            );
        }
        echo json_encode($response);
    }

    public function fetchAllSemByID($payload){ //department_list
            $this->db2->where('sem_ID', $payload['sem_ID']);
           $query = $this->db2->get('semester');
         
           $result = $query->row();
           
           if($query->num_rows() == 1){
              $response = array('status' => 'SUCCESS',
                                'message' => 'SUCCESS FETCHING DATA',
                                 'payload' => $result); // if success
             
              echo json_encode($response);
           }
           else{
              $response = array('status' => 'FAILED',
                                'message' => 'FAILED FETCHING DATA'); // if failed
              echo json_encode($response);
          }
   }
    
// ================ WEB SERVICES FOR PAYMENT OPTIONS
    
    public function fetchAllPayment(){
    	$sql = $this->db2->get("payment_options po");
    	$this->db2->select("*");
        $this->db2->from("payment_options po");
        $this->db2->join("semester sem", "po.sem_ID = sem.sem_ID");
        $sql = $this->db2->get();
        if($sql){
    		$response = array(
    					'status'=>'SUCCESS',
    					'message'=>'SUCCESS FETCHING',
    					'payload'=>$sql->result()
    		);
    	}else{
    		$response = array(
    					'status'=>'ERROR',
    					'message'=>'ERROR IN SQL'
    		);
    	}
    	echo json_encode($response);
    }
    
    public function insertNewPaymentOption($payload){
       $sql = $this->db2->get_where("semester", array("sem_ID"=>$payload['sem_ID']));
       if($sql->num_rows() > 0){
           $row = $sql->row();
           $data = array(
                   'sem_ID'=>$row->sem_ID,
                   'po_NAME'=>$payload['po_NAME'],
                   'po_book_fees'=>$payload['po_book_fees'],
                   'po_lab_fees'=>$payload['po_lab_fees'],
                   'po_regother_fees'=>$payload['po_regother_fees'],
                   'po_LAB'=>$payload['po_LAB'],
                   'po_LEC'=>$payload['po_LEC'],
                   'po_MISC'=>$payload['po_MISC'],
                   'po_DATE1'=>$payload['po_DATE1'],
                   'po_DATE2'=>$payload['po_DATE2'],
                   'po_DATE3'=>$payload['po_DATE3'],
                   'po_DATE4'=>$payload['po_DATE4'],
                   'po_DATE5'=>$payload['po_DATE5'],
                   'po_discount_tf'=>$payload['po_discount_tf'],
                   'po_discount_misc'=>$payload['po_discount_misc'],
                   'po_installment_fee'=>$payload['po_installment_fee'],
                   'po_additional_tf'=>$payload['po_additional_tf'],
                   'po_additional_misc'=>$payload['po_additional_misc'],
                   'po_additional_tfee'=>$payload['po_additional_tfee'],
                   'po_SNPL'=>$payload['po_SNPL'],
           );
           $insert = $this->db2->insert("payment_options",$data);
           if($insert){
               $response = array(
                           'status'=>'SUCCESS',
                           'message'=>'SUCCESS INERTING DATA',
                           'payload'=>$data
               );
               
           }else{
               $response = array(
                       'status'=>'ERROR',
                       'message'=>'ERROR IN INSERTING DATA'
               );
               
           }
       }else{
           $response = array(
                       'status'=>'ERROR',
                       'message'=>'SEM ID NOT EXISTING'
           );
       }
       return json_encode($response);
   }
   public function updatePaymentOption($payload){
       $this->db2->select("*");
       $this->db2->from("payment_options");
       $this->db2->where("po_ID",$payload['po_ID']);
       $sql = $this->db2->get();
       if($sql->num_rows() > 0 ){
           $row = $sql->row();
           $data = array(
                   'po_ID'=>$payload['po_ID'],
                   'sem_ID'=>$payload['sem_ID'],
                   'po_NAME'=>$payload['po_NAME'],
                   'po_book_fees'=>$payload['po_book_fees'],
                   'po_lab_fees'=>$payload['po_lab_fees'],
                   'po_regother_fees'=>$payload['po_regother_fees'],
                   'po_LAB'=>$payload['po_LAB'],
                   'po_LEC'=>$payload['po_LEC'],
                   'po_MISC'=>$payload['po_MISC'],
                   'po_DATE1'=>$payload['po_DATE1'],
                   'po_DATE2'=>$payload['po_DATE2'],
                   'po_DATE3'=>$payload['po_DATE3'],
                   'po_DATE4'=>$payload['po_DATE4'],
                   'po_DATE5'=>$payload['po_DATE5'],
                   'po_discount_tf'=>$payload['po_discount_tf'],
                   'po_discount_misc'=>$payload['po_discount_misc'],
                   'po_installment_fee'=>$payload['po_installment_fee'],
                   'po_additional_tf'=>$payload['po_additional_tf'],
                   'po_additional_misc'=>$payload['po_additional_misc'],
                   'po_additional_tfee'=>$payload['po_additional_tfee'],
                   'po_SNPL'=>$payload['po_SNPL']
           );
           $this->db2->where("po_ID", $row->po_ID);
           $update = $this->db2->update("payment_options",$data);
           if($update){
               $response = array(
                           'status'=>'SUCCESS',
                           'message'=>'SUCCESS UPDATING',
                           'payload'=>$data
               );
           }else{
               $response = array(
                           'status'=>'ERROR',
                           'message'=>'ERROR UPDATING'
               );
           }
       }else{
           $response = array(
                       'status'=>'ERROR',
                       'message'=>'ERROR'
           );
       }
       return json_encode($response);
   }
    
    public function fetchPaymentById($payload){
        //$sql = $this->db2->get_where("payment_options", array("po_ID"=> $payload['po_ID']));
        $this->db2->select("*");
        $this->db2->from("payment_options po");
        $this->db2->join("semester sem", "po.sem_ID = sem.sem_ID", "left");
        $this->db2->where("po_ID", $payload['po_ID']);
        $sql = $this->db2->get();
        if($sql->num_rows() > 0 ){
            $response = array(
                        'status'=> 'SUCCESS',
                        'message'=> 'SUCCESS FETCHING',
                        'payload'=> $sql->result()
            );
        }else{
            $response = array(
                        'status'=>'ERROR',
                        'message'=>'NO DATA'
            );
        }
        echo json_encode($response);
    }
    
    public function insertStudentInfo($payload){
       if($payload == null){
           $response = array('status' => 'FAILED',
                             'message' => 'PLEASE CHECK YOUR DATA');
           echo json_encode($response);
       }

       elseif($payload != null){

                $query = $this->db->where('applicant_id', $payload['applicant_id']);
                $query = $this->db->get('applicant');
                $studentinfo = $query->row();

                $shortYear = date("y");

                $query = $this->db2->query("SELECT MAX(student_ID) AS highest FROM student_information");

                $row = $query->row();
                $id = substr($row->highest,7) + 1;
                $idd = str_pad($id,3,"0",STR_PAD_LEFT);

                $studentID = "MMMA"."-".date("y").$idd;

                $data = array(
                    'si_FNAME' => $studentinfo->applicant_fname,
                    'si_MNAME' => $studentinfo->applicant_mname,
                    'si_LNAME' => $studentinfo->applicant_lname,
                    'student_ID' => $studentID,
                    'si_EMAIL' => $studentinfo->applicant_email,
                    'password' => $studentinfo->applicant_password,
                    'student_type_ID' => $studentinfo->student_type_ID,
                    'address' => $studentinfo->applicant_address,
                    'si_CONTACT' => $studentinfo->applicant_contact,
                    'si_BIRTHYEAR' => $studentinfo->applicant_BIRTHYEAR,
                    'si_BIRTHMONTH' => $studentinfo->applicant_BIRTHMONTH,
                    'si_BIRTHDAY' => $studentinfo->applicant_BIRTHDAY,
                    'si_BIRTHPLACE' => $studentinfo->applicant_BIRTHPLACE,
                    'si_GENDER' => $studentinfo->applicant_GENDER,
                    'si_STREET' => $studentinfo->applicant_STREET,
                    'si_BRGY' => $studentinfo->applicant_BRGY,
                    'si_CITY' => $studentinfo->applicant_CITY,
                    'si_DISTRICT' => $studentinfo->applicant_DISTRICT,
                    'si_PROVINCE' => $studentinfo->applicant_PROVINCE,
                    'course_ID' => $studentinfo->course_ID,
                    'access_ID' => 6
                );

                $this->db2->insert('student_information', $data);
                $student_add_ID = $this->db2->insert_id();
                
                $datas = array(
                    'si_ID' => $student_add_ID,
                    'si_FNAME' => $studentinfo->applicant_fname,
                    'si_FNAME' => $studentinfo->applicant_fname,
                    'si_MNAME' => $studentinfo->applicant_mname,
                    'si_LNAME' => $studentinfo->applicant_lname,
                    'student_ID' => $studentID,
                    'si_EMAIL' => $studentinfo->applicant_email,
                    'password' => $studentinfo->applicant_password,
                    'student_type_ID' => $studentinfo->student_type_ID,
                    'address' => $studentinfo->applicant_address,
                    'si_CONTACT' => $studentinfo->applicant_contact,
                    'si_BIRTHYEAR' => $studentinfo->applicant_BIRTHYEAR,
                    'si_BIRTHMONTH' => $studentinfo->applicant_BIRTHMONTH,
                    'si_BIRTHDAY' => $studentinfo->applicant_BIRTHDAY,
                    'si_BIRTHPLACE' => $studentinfo->applicant_BIRTHPLACE,
                    'si_GENDER' => $studentinfo->applicant_GENDER,
                    'si_STREET' => $studentinfo->applicant_STREET,
                    'si_BRGY' => $studentinfo->applicant_BRGY,
                    'si_CITY' => $studentinfo->applicant_CITY,
                    'si_DISTRICT' => $studentinfo->applicant_DISTRICT,
                    'si_PROVINCE' => $studentinfo->applicant_PROVINCE,
                    'course_ID' => $studentinfo->course_ID,
                    'access_ID' => 6
                );

                $this->db2->insert('request_approval', $datas);
                $query = $this->db->where('applicant_id', $payload['applicant_id']);
                $this->db->delete('applicant');

                $query1 = $this->db->where('aai_ID', $payload['aai_ID']);
                $query1 = $this->db->get('applicant_additional_info');
                $studentinfo1 = $query1->row();

                $deta = array(
                    'si_ID' => $student_add_ID,
                    'sai_name_of_guardian' => $studentinfo1->aai_name_of_guardian, 
                    'sai_relationship' => $studentinfo1->aai_relationship,
                    'sai_occupation' => $studentinfo1->aai_occupation,
                    'sai_tel_cell_number' => $studentinfo1->aai_tel_cell_number,
                    'sai_address' => $studentinfo1->aai_address,
                    'sai_elementary_graduated' => $studentinfo1->aai_elementary_graduated,
                    'sai_elementary_year_graduated' => $studentinfo1->aai_elementary_year_graduated,
                    'sai_highschool_graduated' => $studentinfo1->aai_highschool_graduated,
                    'sai_highschool_year_graduated' => $studentinfo1->aai_highschool_year_graduated,
                    'sai_college_last_attended' => $studentinfo1->aai_college_last_attended,
                    'sai_cla_inclusive_date' => $studentinfo1->aai_cla_inclusive_date,
                    'sai_degree_title' => $studentinfo1->aai_degree_title,
                    'sai_status' => $studentinfo1->aai_status,
                    'sai_graduating' => $studentinfo1->aai_graduating,
                    'sai_religion' => $studentinfo1->aai_religion,
                    'sai_civil' => $studentinfo1->aai_civil,
                    // 'sai_age' => $studentinfo1->aai_age,
                    'sai_height' => $studentinfo1->aai_height,
                    'sai_weight' => $studentinfo1->aai_weight,
                    'sai_special_skills' => $studentinfo1->aai_special_skills,
                    'sai_present_addr_st' => $studentinfo1->aai_present_addr_st,
                    'sai_present_brgy' => $studentinfo1->aai_present_brgy,
                    'sai_present_city' => $studentinfo1->aai_present_city,
                    'sai_present_district' => $studentinfo1->aai_present_district,
                    'sai_present_prov' => $studentinfo1->aai_present_prov,
                    'sai_present_contact' => $studentinfo1->aai_present_contact,
                ); // to get the entered data
                

                $this->db2->insert('student_additional_info', $deta);
                $this->db2->insert('request_approval_add_info', $deta);
                $query1 = $this->db->where('aai_ID', $payload['aai_ID']);
                $this->db->delete('applicant_additional_info');

                $this->db->where('applicant_id',$payload['applicant_id']);
                $delete = $this->db->delete('application');

               /* 
                for migration of selected course
               $selected_course = '';
                $query1 = $this->db2->query("SELECT * FROM curricullum where course_ID = '$selected_course'");
                */



                $response = array(
                   'status'=>'SUCCESS',
                   'message'=>'SUCCESSFULLY INSERTED',
                   'payload'=>$data,
                   'payload1'=>$deta,
                );
               echo json_encode($response);
       }
       else{
           $response = array('status' => 'ERROR',
                             'message' => 'ERROR ENCOUNTERED');
           echo json_encode($response);
       }
   }
    
    public function updateApplicantInfo($payload){
        if($payload == null){
            $response = array('status' => 'FAILED',
                              'message' => 'PLEASE CHECK YOUR DATA');
            echo json_encode($response);
        }

        elseif($payload != null){
            $this->db->where('applicant_id', $payload['applicant_id']);
            $data = $this->db->get('applicant');

            $num = $data->num_rows();

            if($num == 1){
                $deta = array(
                    'applicant_fname' => $payload['applicant_fname'],
                    'applicant_mname' => $payload['applicant_mname'], 
                    'applicant_lname' => $payload['applicant_lname'], 
                    'applicant_email' => $payload['applicant_email'],
                    'applicant_password' => base64_encode($payload['applicant_password']),
                    'applicant_address' => $payload['applicant_address'],
                    'applicant_contact' => $payload['applicant_contact'],
                    'applicant_BIRTHYEAR' => $payload['applicant_BIRTHYEAR'],
                    'applicant_BIRTHMONTH' => $payload['applicant_BIRTHMONTH'],
                    'applicant_BIRTHDAY' => $payload['applicant_BIRTHDAY'],
                    'applicant_BIRTHPLACE' => $payload['applicant_BIRTHPLACE'],
                    'applicant_GENDER' => $payload['applicant_GENDER'],
                    'applicant_STREET' => $payload['applicant_STREET'],
                    'applicant_BRGY' => $payload['applicant_BRGY'],
                    'applicant_CITY' => $payload['applicant_CITY'],
                    'applicant_DISTRICT' => $payload['applicant_DISTRICT'],
                    'applicant_PROVINCE' => $payload['applicant_PROVINCE'],
                    'applicant_online_status' => $payload['applicant_online_status']
                );

                $this->db->update('applicant', $deta, array('applicant_id' => $payload['applicant_id']));

                $response = array('status' => 'SUCCESS',
                                  'message' => 'SUCCESS UPDATING USER DATA');
                echo json_encode($response);
            }

            elseif($num == 0){
                $response = array('status' => 'FAILED',
                                  'message' => 'USER DOESN\'T EXIST');
                echo json_encode($response);
            }

            else{
                $response = array('status' => 'ERROR',
                                  'message' => 'ERROR ENCOUNTERED');
                echo json_encode($response);   
            }
        }

        else{
            $response = array('status' => 'ERROR',
                              'message' => 'ERROR ENCOUNTERED');
            echo json_encode($response);   
        }
    }

    public function insert_audit_trail($payload){

        if($payload == null){
            $response = array('status' => 'FAILED',
                              'message' => 'PLEASE CHECK YOUR DATA');
            echo json_encode($response);
        }

        elseif($payload != null){

          $datetime = date('Y-m-d H:i:s');
          $data = array(
                  'ACCOUNT_ID'=>$payload['ACCOUNT_ID'],
                  'CLIENT_IP'=>$payload['CLIENT_IP'],
                  'ACTION_EVENT'=>$payload['ACTION_EVENT'],
                  'EVENT_DESCRIPTION'=>$payload['EVENT_DESCRIPTION'],
                  'EVENT_DATETIME'=>$datetime
          );
          $sql = $this->db2->insert("audit_trail", $data);
          if($sql){
              $response = array(
                          'status'=>'SUCCESS',
                          'message'=>'SUCCESS INSERTING DATA',
                          'payload'=>$data
              );
          }else{
             $response = array(
                     'status'=>'ERROR',
                     'message'=>'ERROR IN INSERTING DATA'
             );
          }
            echo json_encode($response);
        }
        else{
            $response = array('status' => 'ERROR',
                              'message' => 'ERROR ENCOUNTERED');
            echo json_encode($response);
        }
    }

    public function updatedAccountLevel($payload){
        if($payload == null){
            $response = array(
                'status' => 'FAILED',
                'message' => 'PLEASE CHECK YOUR DATA'
            );
            echo json_encode($response);
        }

        elseif($payload != null){
            $this->db2->where('access_NAME', $payload['access_NAME']);
            $get = $this->db2->get('account_level');

            if($get->num_rows() > 0){
                
                $response = array(
                    'status' => 'ERROR',
                    'message' => 'ACCOUNT ALREADY EXIST'
                );
                echo json_encode($response);
            }
            else{
            
                $data = array(
                    'access_NAME' => strtoupper($payload['access_NAME'])
                );
                $this->db2->update('account_level', $data, array('access_ID' => $payload['access_ID']));
                
                $response = array(
                    'status' => 'SUCCESS',
                    'message' => 'SUCCESS UPDATING USERTYPES'
                );
                echo json_encode($response);
            }
        }
        else{
            $response = array(
                'status' => 'ERROR',
                'message' => 'ERROR'
            );
            echo json_encode($response);
        }     

    }

    public function fetchAllUserTypes(){

        $sql = $this->db2->get('user_account_control'); 

            if($sql->num_rows()>0) {
                $data = $sql->result();
                $response = array('status' => 'SUCCESS',
                                   'message' => 'SUCCESS FETCHING DATA',
                                   'payload' => $data);
               
                echo json_encode($response);
            }
            else{
                $response = array('status' => 'FAILED',
                                   'message' => 'FAILED RETRIEVING DATA');
                 echo json_encode($response);
            }      

    }

    public function updateUserTypesStatus($payload){
        if($payload == null){
            $response = array(
                'status' => 'FAILED',
                'message' => 'PLEASE CHECK YOUR DATA'
            );
            echo json_encode($response);
        }
        elseif($payload != null){
            $this->db2->where('user_account_ID', $payload['user_account_ID']);
            $get = $this->db2->get('user_account_level_control');
            
            if($get->num_rows() > 0){
                $data = array(
                    'status' => $payload['status']
                );
                $this->db2->update('user_account_level_control', $data, array('user_account_ID' => $payload['user_account_ID']));
                
                $response = array(
                    'status' => 'SUCCESS',
                    'message' => 'SUCCESS UPDATING USERTYPES'
                );
                echo json_encode($response);
            }
            else{
                $response = array(
                    'status' => 'ERROR',
                    'message' => 'ACCOUNT DOES NOT EXIST'
                );
                echo json_encode($response);
            }
        }
        else{
            $response = array(
                'status' => 'ERROR',
                'message' => 'ERROR'
            );
            echo json_encode($response);
        }     

    }

    public function updateUserTypesModuleStatus($payload){
        if($payload == null){
            $response = array(
                'status' => 'FAILED',
                'message' => 'PLEASE CHECK YOUR DATA'
            );
            echo json_encode($response);
        }
        elseif($payload != null){
            $this->db2->where('UALCM_ID', $payload['UALCM_ID']);
            $get = $this->db2->get('user_account_level_control_modules');
            
            if($get->num_rows() > 0){
                $data = array(
                    'status' => $payload['status']
                );
                $this->db2->update('user_account_level_control_modules', $data, array('UALCM_ID' => $payload['UALCM_ID']));
                
                $response = array(
                    'status' => 'SUCCESS',
                    'message' => 'SUCCESS UPDATING USERTYPES MODULES'
                );
                echo json_encode($response);
            }
            else{
                $response = array(
                    'status' => 'ERROR',
                    'message' => 'ACCOUNT DOES NOT EXIST'
                );
                echo json_encode($response);
            }
        }
        else{
            $response = array(
                'status' => 'ERROR',
                'message' => 'ERROR'
            );
            echo json_encode($response);
        }     

    }

    public function fetchAllUserModuleByUserTypeID($payload){

        $this->db2->select('*');
        $this->db2->from('user_account_level_control_modules');
        $this->db2->join('user_account_control_modules', 'user_account_control_modules.USER_ACCOUNT_MODULE_ID = user_account_level_control_modules.USER_ACCOUNT_MODULE_ID');
        $this->db2->where('access_ID', $payload['access_ID']);
           
        $query = $this->db2->get();
           
        $result = $query->result();  
        if($query->num_rows() > 0){
            $response = array('status' => 'SUCCESS',
                              'message' => 'SUCCESS FETCHING DATA',
                              'payload' => $result); // if success
               
        }
        else{
            $response = array('status' => 'FAILED',
                              'message' => 'FAILED RETRIEVING DATA'); // if failed  
        }
        echo json_encode($response);
    }


    public function fetAllUserAccountLevelControlByAccessID($payload){

        $this->db2->select('*');
        $this->db2->from('user_account_level_control');
        $this->db2->join('user_account_control', 'user_account_control.USER_TYPE_ID = user_account_level_control.USER_TYPE_ID');
        $this->db2->where('access_ID', $payload['access_ID']);

        $query = $this->db2->get();
           
        $result = $query->result();
            
        if($query->num_rows() > 0){
            $response = array('status' => 'SUCCESS',
                              'message' => 'SUCCESS FETCHING DATA',
                              'payload' => $result); // if success
               
        }
        else{
            $response = array('status' => 'FAILED',
                              'message' => 'FAILED RETRIEVING DATA'); // if failed  
        }
        echo json_encode($response);
    }

    public function fetchallCourseList(){

        $sql = $this->db2->get('course_list'); 

            if($sql->num_rows()>0) {
                $data = $sql->result();
                $response = array('status' => 'SUCCESS',
                                   'message' => 'SUCCESS FETCHING DATA',
                                   'payload' => $data);
               
                echo json_encode($response);
            }
            else{
                $response = array('status' => 'FAILED',
                                   'message' => 'FAILED RETRIEVING DATA');
                 echo json_encode($response);
            }      
    }
    public function update($payload){

        $secondsql = $this->db2->query("SELECT semester_section_id FROM semester_section WHERE sem_ID ='".$payload['sem_ID']."' ");
        $secondresult = $secondsql->result_array(); 

        $values = array_map('array_pop', $secondresult);
        $imploded = "'".implode("','", $values)."'";
        // var_dump($imploded);
        $this->db2->query("UPDATE subject_sched SET is_lock =".$payload['lock']." , is_check = ".$payload['check']." WHERE semester_section_id IN (".$imploded.") ");
            
        $response = array('status' => 'SUCCESS',
                                       'message' => 'SUCCESS FETCHING DATA',
                                       'payload' => $imploded);
        echo json_encode($response);

    }

    public function updatelock_unlock($payload){

        $secondsql = $this->db2->query("SELECT semester_section_id FROM semester_section WHERE sem_ID ='".$payload['sem_ID']."' ");
        $secondresult = $secondsql->result_array(); 

        $values = array_map('array_pop', $secondresult);
        $imploded = "'".implode("','", $values)."'";
        // var_dump($imploded);
        $this->db2->query("UPDATE subject_sched SET is_lock =".$payload['lock']." WHERE semester_section_id IN (".$imploded.") ");
            
        $response = array('status' => 'SUCCESS',
                                       'message' => 'SUCCESS FETCHING DATA',
                                       'payload' => $imploded);
        echo json_encode($response);

    }

    public function updatecheck_uncheck($payload){

        $secondsql = $this->db2->query("SELECT semester_section_id FROM semester_section WHERE sem_ID ='".$payload['sem_ID']."' ");
        $secondresult = $secondsql->result_array(); 

        $values = array_map('array_pop', $secondresult);
        $imploded = "'".implode("','", $values)."'";
        // var_dump($imploded);
        $this->db2->query("UPDATE subject_sched SET is_check = ".$payload['check']." WHERE semester_section_id IN (".$imploded.") ");
            
        $response = array('status' => 'SUCCESS',
                                       'message' => 'SUCCESS FETCHING DATA',
                                       'payload' => $imploded);
        echo json_encode($response);

    }
    public function updateTrisemStatus($payload){

        if($payload == null){
            $response = array(
                'status' => 'FAILED',
                'message' => 'PLEASE CHECK YOUR DATA'
            );
            echo json_encode($response);
        }
        elseif($payload != null){
            
                $this->db2->where('sem_ID', $payload['sem_ID']);
                $get = $this->db2->get('semester');
                
                $data = array(
                   'sem_lock_unlocked_check_unchecked'=>$payload['sem_lock_unlocked_check_unchecked']
                );
                
                $this->db2->update('semester', $data, array('sem_ID' => $payload['sem_ID']));
                
                $response = array(
                    'status' => 'SUCCESS',
                    'message' => 'SUCCESS UPDATING DATA',
                    'payload' => $payload
                );
                echo json_encode($response);    
        }
        else{
            $response = array(
                'status' => 'ERROR',
                'message' => 'ERROR'
            );
            echo json_encode($response);
        }
    
    }

    public function insertTrimester($payload){
        if($payload == null){
            $response = array('status' => 'FAILED',
                              'message' => 'PLEASE CHECK YOUR DATA');
            echo json_encode($response);
        }

        elseif($payload != null){

            $data = array(
                   'sem_NAME'=>$payload['sem_NAME'],
                   'sem_STATUS'=>$payload['sem_STATUS'],
                   'sem_ENROLMENT'=>$payload['sem_ENROLMENT'],
                   'sem_ENCODING'=>$payload['sem_ENCODING'],
                   'sem_LOADING'=>$payload['sem_LOADING'],
            );

            $sem_NAME = $payload['sem_NAME'];
            $this->db2->where('sem_NAME', $sem_NAME);

            $query = $this->db2->get('semester');
            $num = $query->num_rows();

            if($num == 1){
                $response = array('status' => 'FAILED',
                                  'message' => 'TRIMESTER ALREADY EXISTS');
                echo json_encode($response);
            }

            else{
                $insert = $this->db2->insert("semester",$data);

                if($insert){
                   $response = array(
                               'status'=>'SUCCESS',
                               'message'=>'SUCCESS INERTING DATA',
                               'payload'=>$data
                   );
                   
                echo json_encode($response);
                }else{
                   $response = array(
                           'status'=>'ERROR',
                           'message'=>'ERROR IN INSERTING DATA'
                   );
                   
                echo json_encode($response);
                }
            }
        }
        else{
            $response = array('status' => 'ERROR',
                              'message' => 'ERROR ENCOUNTERED');
            echo json_encode($response);
        }
   }
    
    public function updateTrimester($payload){
        if($payload == null){
            $response = array(
                'status' => 'FAILED',
                'message' => 'PLEASE CHECK YOUR DATA'
            );
            echo json_encode($response);
        }
        elseif($payload != null){
            $this->db2->where('sem_NAME', $payload['sem_NAME']);
            $get = $this->db2->get('semester');
            
            // if($get->num_rows() > 0){
            //     $response = array(
            //         'status' => 'ERROR',
            //         'message' => 'TRIMESTER NAME ALREADY EXISTS'
            //     );
            //     echo json_encode($response);
            // }
            // else{
                $this->db2->where('sem_ID', $payload['sem_ID']);
                $get = $this->db2->get('semester');
                $row = $get->row();
                $data = array(
                    'sem_NAME'=>$payload['sem_NAME'],
                    'sem_STATUS'=>$payload['sem_STATUS'],
                    'sem_ENROLMENT'=>$payload['sem_ENROLMENT'],
                    'sem_ENCODING'=>$payload['sem_ENCODING'],
                    'sem_LOADING'=>$payload['sem_LOADING'],
                    'sem_lock_unlocked_check_unchecked' => $row->sem_lock_unlocked_check_unchecked
                );

                $this->db2->update('semester', $data, array('sem_ID' => $payload['sem_ID']));
                
                $response = array(
                    'status' => 'SUCCESS',
                    'message' => 'SUCCESS UPDATING DATA',
                    'payload' => $payload
                );
                echo json_encode($response);    
            // }
        }
        else{
            $response = array(
                'status' => 'ERROR',
                'message' => 'ERROR'
            );
            echo json_encode($response);
        }
    }
    public function UpsertUserType($payload)
    {

         if($payload == null){
            $response = array(
                'status' => 'FAILED',
                'message' => 'PLEASE CHECK YOUR DATA'
            );
            echo json_encode($response);
        }
        elseif($payload != null){

             $check = $this->db2->query("SELECT access_NAME FROM account_level WHERE access_NAME ='".strtoupper($payload['access_NAME'])."'");
          
             if(count($check->result()) > 0 )
             {
                 $response = array('status' => 'FAILED',
                                 'message' => 'ALREADY EXIST');

                 echo json_encode($response);
                 exit();

            }else{
             $this->db2->insert('account_level',array('access_NAME' => strtoupper($payload['access_NAME'])));
             $access_id = $this->db2->insert_id();
             $sql = $this->db2->query("SELECT USER_TYPE_ID FROM user_account_control");
             $result = $sql->result_array();
            

            // // get and count user_account_control 
            for ($i=0; $i <count($sql->result()) ; $i++) 
             { 
                 $firstdata[] = array('access_ID' => $access_id,
                 'USER_TYPE_ID'  => $result[$i]['USER_TYPE_ID'],
                 'status' => '2'
                );
             }
            $this->db2->insert_batch('user_account_level_control',$firstdata);
            $sql1 = $this->db2->query("SELECT * FROM user_account_modules");
            $result1 = $sql1->result_array();

            for ($j=0; $j <count($sql1->result()) ; $j++) { 
                $secondata[] = array(
                      'USER_TYPE_ID'  => $result1[$j]['USER_TYPE_ID'],  
                      'access_ID' => $access_id,
                      'USER_ACCOUNT_MODULE_ID'  => $result1[$j]['USER_ACCOUNT_MODULE_ID'],
                      'status' => '2'
                );
            }
             $this->db2->insert_batch('user_account_level_control_modules',$secondata);

             $response = array(
                           'status'=>'SUCCESS',
                           'message'=>'SUCCESS INSERTING DATA'
                   );
                   
               }

        }else{
            $response = array(
                'status' => 'ERROR',
                'message' => 'ERROR'
            );
          
        }

          echo json_encode($response);
    }

    public function fetchSubjectSched($payload){

   
    $this->db2->where('subject_ID', $payload['subject_ID']);
       $query = $this->db2->get('subject_sched');
     
       $result = $query->row();
       
       if($query->num_rows() == 1){
          $response = array('status' => 'SUCCESS',
                            'message' => 'SUCCESS FETCHING DATA',
                             'payload' => $result); // if success
         
          echo json_encode($response);
       }
       else{
          $response = array('status' => 'FAILED',
                            'message' => 'FAILED FETCHING DATA'); // if failed
          echo json_encode($response);
      }
    }

    public function insert($payload){

      for($i=0; $i < $payload['value']; $i++){
        for($j=1;$j< 6;$j++){

          $data2 = array(
            "si_ID" => $i+1,
            "req_ID" => $j,
            "status" => "1"
            );

          $this->db2->insert('student_requirements_passed', $data2);
        }
      }
      $response = array(
                    'status'=>'SUCCES',
                           'message'=>'SUCCESS INSERTING DATA',
                           'payload' => $data2
                   );
      echo json_encode($response);
    }

    public function getHighest(){
        $query = $this->db2->query("SELECT MAX(student_ID) AS highest FROM student_information");
        $row = $query->row();
        
        $id = substr($row->highest,7) + 1;
        $response = array(
                    'status'=>'SUCCES',
                           'message'=>'SUCCESS INSERTING DATA',
                           'payload' => $id
                   );
        echo json_encode($response);
    }

    public function setInActivetiActive(){
        $query = $this->db2->query("SELECT `account_ID` FROM account_info as ai WHERE (unix_timestamp(NOW()) - (SELECT unix_timestamp(EVENT_DATETIME) FROM audit_trail as at WHERE at.ACCOUNT_ID = ai.ACCOUNT_ID ORDER BY AUDIT_ID DESC LIMIT 1)) >= 20");
        $row = $query->result_array();
        $values = array_map('array_pop', $row);
        $imploded =  "'" . implode("','", $values) . "'"; 
        $this->db2->query("UPDATE `account_info` SET `access_status` = '1' WHERE account_ID IN(".$imploded.")");			
        if($query)
        {
            $response = array('payload'=> $row, 'status' => 'SUCCESS',
                'message' => 'SUCCESSFULLY SAVE');
        }else{

            $response = array('status' => 'FAILED',
					'message' => 'FAILED TO SAVE!');
        }
        echo json_encode($response);
    }

    public function upsertRequestApproval($payload){
        if($payload == null){
            $response = array('status' => 'FAILED',
                              'message' => 'PLEASE CHECK YOUR DATA');
            echo json_encode($response);
        }
 
        elseif($payload != null){
                 
                 $query = $this->db2->where('si_ID', $payload['si_ID']);
                 $query = $this->db2->get('request_approval');
                 $studentinfo = $query->row();
                // var_dump ($studentinfo);
                 $data = array(
                     'si_ID' => $studentinfo->si_ID,
                     'si_FNAME' => $studentinfo->si_FNAME,
                     'si_MNAME' => $studentinfo->si_MNAME,
                     'si_LNAME' => $studentinfo->si_LNAME,
                     'student_ID' => $studentinfo->student_ID,
                     'si_EMAIL' => $studentinfo->si_EMAIL,
                     'password' => $studentinfo->password,
                     'student_type_ID' => $studentinfo->student_type_ID,
                     'address' => $studentinfo->address,
                     'si_CONTACT' => $studentinfo->si_CONTACT,
                     'si_BIRTHYEAR' => $studentinfo->si_BIRTHYEAR,
                     'si_BIRTHMONTH' => $studentinfo->si_BIRTHMONTH,
                     'si_BIRTHDAY' => $studentinfo->si_BIRTHDAY,
                     'si_BIRTHPLACE' => $studentinfo->si_BIRTHPLACE,
                     'si_GENDER' => $studentinfo->si_GENDER,
                     'si_STREET' => $studentinfo->si_STREET,
                     'si_BRGY' => $studentinfo->si_BRGY,
                     'si_CITY' => $studentinfo->si_CITY,
                     'si_DISTRICT' => $studentinfo->si_DISTRICT,
                     'si_PROVINCE' => $studentinfo->si_PROVINCE,
                     'course_ID' => $studentinfo->course_ID,
                     'access_ID' => 6,
                     'request_status' => $studentinfo->request_status
                 );
 
                 $query = $this->db2->where('si_ID', $payload['si_ID']);
                 $this->db2->update('student_information', $data);
 
                 $query1 = $this->db2->where('RAAI_ID', $payload['RAAI_ID']);
                 $query1 = $this->db2->get('request_approval_add_info');
                 $studentinfo1 = $query1->row();
 
                 $deta = array(
                     'si_ID' => $studentinfo1->si_ID,
                     'sai_name_of_guardian' => $studentinfo1->sai_name_of_guardian, 
                     'sai_relationship' => $studentinfo1->sai_relationship, 
                     'sai_occupation' => $studentinfo1->sai_occupation,
                     'sai_tel_cell_number' => $studentinfo1->sai_tel_cell_number,
                     'sai_address' => $studentinfo1->sai_address,
                     'sai_elementary_graduated' => $studentinfo1->sai_elementary_graduated,
                     'sai_elementary_year_graduated' => $studentinfo1->sai_elementary_year_graduated,
                     'sai_highschool_graduated' => $studentinfo1->sai_highschool_graduated,
                     'sai_highschool_year_graduated' => $studentinfo1->sai_highschool_year_graduated,
                     'sai_college_last_attended' => $studentinfo1->sai_college_last_attended,
                     'sai_cla_inclusive_date' => $studentinfo1->sai_cla_inclusive_date,
                     'sai_degree_title' => $studentinfo1->sai_degree_title,
                     'sai_status' => $studentinfo1->sai_status,
                     'sai_graduating' => $studentinfo1->sai_graduating,
                     'sai_religion' => $studentinfo1->sai_religion,
                     'sai_civil' => $studentinfo1->sai_civil,
                     // 'sai_age' => $studentinfo1->sai_age,
                     'sai_height' => $studentinfo1->sai_height,
                     'sai_weight' => $studentinfo1->sai_weight,
                     'sai_special_skills' => $studentinfo1->sai_special_skills,
                     'sai_present_addr_st' => $studentinfo1->sai_present_addr_st,
                     'sai_present_brgy' => $studentinfo1->sai_present_brgy,
                     'sai_present_city' => $studentinfo1->sai_present_city,
                     'sai_present_district' => $studentinfo1->sai_present_district,
                     'sai_present_prov' => $studentinfo1->sai_present_prov,
                     'sai_present_contact' => $studentinfo1->sai_present_contact,
                 ); // to get the entered data
                 
                 $query1 = $this->db2->where('sai_ID', $payload['RAAI_ID']);
                 $this->db2->update('student_additional_info', $deta);
                
                 $response = array(
                   'status'=>'SUCCESS',
                   'message'=>'SUCCESSFULLY UPDATED',
                   'payload'=>$data,
                   'payload1'=>$deta,
                );
               echo json_encode($response);
        }
 
     }
}


?>
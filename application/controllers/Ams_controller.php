<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");

class Ams_controller extends CI_Controller{
    
// ================ WEB SERVICES FOR DEPARTMENT AND STUDENT
    
    public function fetchAllDepartmentList(){
        echo $this->ams_model->fetchAllDepartmentList();
    }

    public function fetchDepartmentListByID(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->ams_model->fetchDepartmentListByID($payload);
    }

    public function fetchStudentInfoByID(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->ams_model->fetchStudentInfoByID($payload);
    }

// ================ WEB SERVICES FOR ACCOUNT INFO AND ACCOUNT LEVEL
    
    public function fetchAllAccount(){
        echo $this->ams_model->fetchAllAccount();
    }
    
    public function fetchAllAccountLevel(){
        echo $this->ams_model->fetchAllAccountLevel();
    }

    public function fetchAccountByAccountID(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->ams_model->fetchAccountByAccountID($payload);
    }

    public function createAccountInfo(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->ams_model->createAccountInfo($payload);
    }

    public function updatePassword(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->ams_model->updatePassword($payload);
    }

    public function updateStudentPassword(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->ams_model->updateStudentPassword($payload);
    }
    
    public function updateAccountInfo(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->ams_model->updateAccountInfo($payload);
    }
    
// ================ WEB SERVICES FOR INSTRUCTOR AND SEM

    public function createClearance(){
        $payload = json_decode(file_get_contents('php://input'), true);
        $this->ams_model->createClearance($payload);
    }

    public function updateClearance(){
        $payload = json_decode(file_get_contents('php://input'), true);
        $this->ams_model->updateClearance($payload);
    }

    public function fetchAllClearance(){
        $payload = json_decode(file_get_contents('php://input'), true);
        $this->ams_model->fetchAllClearance($payload);
    }

    public function fetchAllClearanceByID(){
        $payload = json_decode(file_get_contents('php://input'), true);
        $this->ams_model->fetchAllClearanceByID($payload);
    }

    public function fetchAllSem(){
        $payload = json_decode(file_get_contents('php://input'), true);
        $this->ams_model->fetchAllSem($payload);
    }

    public function fetchAllSemByID(){
        $payload = json_decode(file_get_contents('php://input'), true);
        $this->ams_model->fetchAllSemByID($payload);
    }

    public function insertTrimester(){
        $payload = json_decode(file_get_contents('php://input'), true);
        $this->ams_model->insertTrimester($payload);
    }

    public function updateTrimester(){
        $payload = json_decode(file_get_contents('php://input'), true);
        $this->ams_model->updateTrimester($payload);
    }

    public function updateTrisemStatus(){
        $payload = json_decode(file_get_contents('php://input'), true);
        $this->ams_model->updateTrisemStatus($payload);
    }

// ================ WEB SERVICES FOR PAYMENT OPTIONS
    
    public function fetchAllPayment(){
        echo $this->ams_model->fetchAllPayment();
    }

    public function insertNewPaymentOption(){
        $payload = json_decode(file_get_contents('php://input'),true);
        echo $this->ams_model->insertNewPaymentOption($payload);
    }

    public function updatePaymentOption(){
        $payload = json_decode(file_get_contents('php://input'),true);
        echo $this->ams_model->updatePaymentOption($payload);
    }

    public function fetchPaymentById(){
        $payload = json_decode(file_get_contents('php://input'),true);
        echo $this->ams_model->fetchPaymentById($payload);
    }

// ================ WEB SERVICES FOR ADDING NEW STUDENT
    
    public function insertStudentInfo(){
        $payload = json_decode(file_get_contents('php://input'),true);
        echo $this->ams_model->insertStudentInfo($payload);
    }
    
    public function insert_audit_trail(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->ams_model->insert_audit_trail($payload);
    }
    
    public function fetchAllUserTypes(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->ams_model->fetchAllUserTypes($payload);
    }
    
    public function updatedAccountLevel(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->ams_model->updatedAccountLevel($payload);
    }
    //  public function fetchAllUserTypes1(){
    //     $payload = json_decode(file_get_contents('php://input'),true);
    //     $this->ams_model->fetchAllUserTypes1($payload);
    // }

    public function fetchAllUserModuleByUserTypeID(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->ams_model->fetchAllUserModuleByUserTypeID($payload);
    }

    public function fetAllUserAccountLevelControlByAccessID(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->ams_model->fetAllUserAccountLevelControlByAccessID($payload);
    }

    public function updateUserTypesStatus(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->ams_model->updateUserTypesStatus($payload);
    }

    public function updateUserTypesModuleStatus(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->ams_model->updateUserTypesModuleStatus($payload);
    }

    public function fetchallCourseList(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->ams_model->fetchallCourseList($payload);
    }

    public function update(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->ams_model->update($payload);
    }

    public function updatelock_unlock(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->ams_model->updatelock_unlock($payload);
    }

    public function updatecheck_uncheck(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->ams_model->updatecheck_uncheck($payload);
    }
    
    public function fetchAllUI(){
        echo $this->ams_model->fetchAllUI();
    }

    public function fetchUIByAccountID(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->ams_model->fetchUIByAccountID($payload);
    }

    public function UpsertUserType()
    {   
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->ams_model->UpsertUserType($payload);

    }

    public function fetchSubjectSched()
    {   
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->ams_model->fetchSubjectSched($payload);

    }

    public function upsertRequestApproval()
    {   
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->ams_model->upsertRequestApproval($payload);

    }
    // ===========================================================================

    public function insert(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->ams_model->insert($payload);
    }

    public function getHighest(){
        //$payload = json_decode(file_get_contents('php://input'),true);
        $this->ams_model->getHighest();
    }
    public function setInActivetiActive(){
        //$payload = json_decode(file_get_contents('php://input'),true);
        $this->ams_model->setInActivetiActive();
    }

}

?>
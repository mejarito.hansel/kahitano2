<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");

class Magsaysay_controller extends CI_Controller {
    
    public function fetchallApplicantInfo(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->fetchallApplicantInfo($payload);
    }
    
    public function fetchallAdditionalApplicantInfo(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->fetchallAdditionalApplicantInfo($payload);
    }
    
    public function submitApplicantInfo(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->submitApplicantInfo($payload);
    }
    
    public function submitAdditionalApplicantInfo(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->submitAdditionalApplicantInfo($payload);
    }
    
    public function updateApplicantInfo(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->updateApplicantInfo($payload);
    }
    
    public function updateAdditionalApplicantInfo(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->updateAdditionalApplicantInfo($payload);
    }
    
    public function fetchApplicantInfoByApplicantStatus(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->fetchApplicantInfoByApplicantStatus($payload);
    }
    
    public function fetchApplicantInfoByApplicantID(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->fetchApplicantInfoByApplicantID($payload);
    }
    
    public function fetchAdditionalApplicantInfoByID(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->fetchAdditionalApplicantInfoByID($payload);
    }
    
    public function updatePassword(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->updatePassword($payload);
    }
 
    public function updateApplicantStatus(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->updateApplicantStatus($payload);
    }
 
    public function updateApplicantStatus1(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->updateApplicantStatus1($payload);
    }
    
    public function getApplicantList(){
        echo $this->magsaysay_model->getApplicantList();
    }
    
    public function getApplicantById(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->getApplicantById($payload);   
    }

    public function getApplicantByNumber(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->getApplicantByNumber($payload);   
    }
    
    public function login(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->login($payload);   
    }
    
    public function submitApplication(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->submitApplication($payload);      
    }
    
    public function displayVoucherInfo(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->displayVoucherInfo($payload);
    }
    
    public function updateExamSchedule(){
        $payload = json_decode(file_get_contents('php://input'),true);
        echo $this->magsaysay_model->updateExamSchedule($payload);    
    }

    public function updateInterviewSchedule(){
        $payload = json_decode(file_get_contents('php://input'),true);
        echo $this->magsaysay_model->updateInterviewSchedule($payload);    
    }

    public function updateInterviewResult(){
        $payload = json_decode(file_get_contents('php://input'),true);
        echo $this->magsaysay_model->updateInterviewResult($payload);    
    }

    public function updateBoaResult(){
        $payload = json_decode(file_get_contents('php://input'),true);
        echo $this->magsaysay_model->updateBoaResult($payload);    
    }

    public function updateBoaSchedule(){
        $payload = json_decode(file_get_contents('php://input'),true);
        echo $this->magsaysay_model->updateBoaSchedule($payload);    
    }

    public function updateMedicalSchedule(){
        $payload = json_decode(file_get_contents('php://input'),true);
        echo $this->magsaysay_model->updateMedicalSchedule($payload);    
    }

    public function updateMedicalResult(){
        $payload = json_decode(file_get_contents('php://input'),true);
        echo $this->magsaysay_model->updateMedicalResult($payload);    
    }
    
    public function updateApplicationStatus(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->updateApplicationStatus($payload);   
    }
    
    public function submitExamTemplate(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->submitExamTemplate($payload);     
    }
    
    public function submitExamTemplateQuestion(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->submitExamTemplateQuestion($payload);     
    }
    
//    public function submitQuestionnaireWithPictureAsAnswer(){
//        echo $this->magsaysay_model->submitQuestionnaireWithPictureAsAnswer();    
//    }
    
//    public function submitQuestionnaireWithTextAsAnswer(){
//        echo $this->magsaysay_model->submitQuestionnaireWithTextAsAnswer();    
//    }
    
//    public function submitReceipt(){
//        echo $this->magsaysay_model->submitReceipt();    
//    }
    
    public function generateAnswerID(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->generateAnswerID($payload);  
    }
    
    public function submitIQ(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->submitIQ($payload);  
    }
    
    public function submitEQ(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->submitEQ($payload);  
    }
    
    public function updateExamTime(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->updateExamTime($payload);
    }
    
    public function countIQScore(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->countIQScore($payload);
    }
    
    public function countEQScore(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->countEQScore($payload);
    }
    
    public function getExamListSortById(){
		echo $this->magsaysay_model->getExamListSortById();
	}

    public function submitExamResult(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->submitExamResult($payload);
    }

    public function updateExamResult(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->updateExamResult($payload);
    }
    
    public function getExamResult(){
        echo $this->magsaysay_model->getExamResult();
    }
    
// -----------------------------------magsaysay_model ends here    
    
    public function getApplicationByApplicantID(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->v1_model->getApplicationByApplicantID($payload);   
    }
    
    public function getApplicationByAcadIDAndApplicantID(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->v1_model->getApplicationByAcadIDAndApplicantID($payload);   
    }
        
    public function getApplicationByStatus(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->v1_model->getApplicationByStatus($payload);
    }
        
    public function getQuestionnaireList(){
        echo $this->v1_model->getQuestionnaireList();
    }
        
    public function getQuestionnaireByID(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->v1_model->getQuestionnaireByID($payload);
    }
        
    public function getExamTemplateList(){
        echo $this->v1_model->getExamTemplateList();
    }
        
    public function getExamTemplateByID(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->v1_model->getExamTemplateByID($payload);
    }
        
    public function getAcademicSemList(){
        echo $this->v1_model->getAcademicSemList();
    }
        
    public function getAcademicSemByStatus(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->v1_model->getAcademicSemByStatus($payload);
    }
    
    public function getAcademicSemByYearAndSem(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->v1_model->getAcademicSemByYearAndSem($payload);   
    }
    
// ==========================================V1 model ends here

    public function insert(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->magsaysay_model->insert($payload);
    }
}

?>
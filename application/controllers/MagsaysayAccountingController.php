<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");

class MagsaysayAccountingController extends CI_Controller {

    public function insertFeeConfig(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->MagsaysayAccountingModel->insertFeeConfig($payload);
    }
    
    public function getAllFeeConfig(){
        echo $this->MagsaysayAccountingModel->getAllFeeConfig();
    }
    
    public function getAllFeeConfigByTemplateID(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->MagsaysayAccountingModel->getAllFeeConfigByTemplateID($payload);
    }
    
    public function getAllFeeConfigByTypeID(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->MagsaysayAccountingModel->getAllFeeConfigByTypeID($payload);
    }
    
    public function getAllFeeConfigByStatus(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->MagsaysayAccountingModel->getAllFeeConfigByStatus($payload);
    }
    
    public function updateFeeConfigRate(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->MagsaysayAccountingModel->updateFeeConfigRate($payload);
    }
    
    public function deleteFeeConfig(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->MagsaysayAccountingModel->deleteFeeConfig($payload);
    }
    
// end of web services for fee config
    
    public function insertFeeTemplate(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->MagsaysayAccountingModel->insertFeeTemplate($payload);
    }
    
    public function getAllFeeTemplate(){
        echo $this->MagsaysayAccountingModel->getAllFeeTemplate();
    }
    
    public function getAllFeeTemplateByID(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->MagsaysayAccountingModel->getAllFeeTemplateByID($payload);
    }
    
    public function getAllFeeTemplateByStatus(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->MagsaysayAccountingModel->getAllFeeTemplateByStatus($payload);
    }
    
    public function deleteFeeTemplate(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->MagsaysayAccountingModel->deleteFeeTemplate($payload);
    }

// end of web services for fee template
    
    public function insertFeeType(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->MagsaysayAccountingModel->insertFeeType($payload);
    }
    
    public function getAllFeeType(){
        echo $this->MagsaysayAccountingModel->getAllFeeType($payload);
    }
    
    public function getAllFeeTypeByID(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->MagsaysayAccountingModel->getAllFeeTypeByID($payload);
    }
    
    public function getAllFeeTypeByStatus(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->MagsaysayAccountingModel->getAllFeeTypeByStatus($payload);
    }
    
    public function updateFeeTypeName(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->MagsaysayAccountingModel->updateFeeTypeName($payload);
    }
    
    public function deleteFeeType(){
        $payload = json_decode(file_get_contents('php://input'),true);
        $this->MagsaysayAccountingModel->deleteFeeType($payload);
    }
}

?>
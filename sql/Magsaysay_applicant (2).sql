-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 30, 2018 at 07:32 PM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Magsaysay_applicant`
--

-- --------------------------------------------------------

--
-- Table structure for table `academic_sem`
--

CREATE TABLE `academic_sem` (
  `acad_id` int(11) NOT NULL,
  `acad_year` text NOT NULL,
  `acad_sem` text NOT NULL,
  `acad_created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `acad_status` int(11) NOT NULL DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `academic_sem`
--

INSERT INTO `academic_sem` (`acad_id`, `acad_year`, `acad_sem`, `acad_created_date`, `acad_status`) VALUES
(1, '2018', '1Y1', '2018-07-25 18:22:22', 2),
(2, '2018', '1Y2', '2018-07-25 18:22:22', 2),
(3, '2018', '1Y3', '2018-07-25 18:22:39', 2),
(4, '2018', '2Y1', '2018-07-25 18:22:39', 2);

-- --------------------------------------------------------

--
-- Table structure for table `applicant`
--

CREATE TABLE `applicant` (
  `applicant_id` int(11) NOT NULL,
  `applicant_fname` text NOT NULL,
  `applicant_lname` text NOT NULL,
  `applicant_email` text NOT NULL,
  `applicant_password` text NOT NULL,
  `applicant_address` text NOT NULL,
  `applicant_contact` text NOT NULL,
  `applicant_joined_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `applicant_status` int(11) NOT NULL DEFAULT '1',
  `applicant_online_status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applicant`
--

INSERT INTO `applicant` (`applicant_id`, `applicant_fname`, `applicant_lname`, `applicant_email`, `applicant_password`, `applicant_address`, `applicant_contact`, `applicant_joined_date`, `applicant_status`, `applicant_online_status`) VALUES
(1, 'John', 'Doe', 'johndoe@yahoo.com', 'asdasdasd', 'asd', '09187263847', '2018-07-30 14:19:09', 1, 0),
(2, 'Jane', 'Doe', 'janedoe@yahoo.com', '123123123', 'Address', '09172673894', '2018-07-30 19:11:28', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `application`
--

CREATE TABLE `application` (
  `application_id` int(11) NOT NULL,
  `acad_id` int(11) NOT NULL,
  `applicant_id` int(11) NOT NULL,
  `application_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `application_number` text NOT NULL,
  `application_payment_details` text NOT NULL,
  `application_status` int(11) NOT NULL DEFAULT '6',
  `application_exam_details` text NOT NULL,
  `application_date_released` datetime NOT NULL,
  `application_exam_score` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `application`
--

INSERT INTO `application` (`application_id`, `acad_id`, `applicant_id`, `application_date`, `application_number`, `application_payment_details`, `application_status`, `application_exam_details`, `application_date_released`, `application_exam_score`) VALUES
(1, 1, 11, '2018-07-25 19:19:59', 'APPLICATION#1', '', 6, '', '0000-00-00 00:00:00', 0),
(2, 2, 11, '2018-07-25 20:37:49', 'APPLICATION#2', '', 6, '', '0000-00-00 00:00:00', 0),
(3, 3, 2, '2018-07-30 19:12:05', 'APPLICATION#3', '', 6, '', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exam_result`
--

CREATE TABLE `exam_result` (
  `exam_result_id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `questionnaire_id` int(11) NOT NULL,
  `questionnaire_result` text NOT NULL,
  `exam_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `exam_template`
--

CREATE TABLE `exam_template` (
  `exam_id` int(11) NOT NULL,
  `exam_name` text NOT NULL,
  `exam_status` int(11) NOT NULL DEFAULT '1',
  `exam_passing_score` int(11) NOT NULL,
  `exam_randomized` int(11) NOT NULL DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_template`
--

INSERT INTO `exam_template` (`exam_id`, `exam_name`, `exam_status`, `exam_passing_score`, `exam_randomized`) VALUES
(6, 'Entrance Exam', 1, 20, 2);

-- --------------------------------------------------------

--
-- Table structure for table `exam_template_questions`
--

CREATE TABLE `exam_template_questions` (
  `exam_template_questions_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `questionnaire_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_template_questions`
--

INSERT INTO `exam_template_questions` (`exam_template_questions_id`, `exam_id`, `questionnaire_id`) VALUES
(1, 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `questionnaire_bank`
--

CREATE TABLE `questionnaire_bank` (
  `questionnaire_id` int(11) NOT NULL,
  `questionnaire_question` text NOT NULL,
  `questionnaire_question_pic` text NOT NULL,
  `questionnaire_answer` text NOT NULL,
  `questionnaire_choices` text NOT NULL,
  `questionnaire_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questionnaire_bank`
--

INSERT INTO `questionnaire_bank` (`questionnaire_id`, `questionnaire_question`, `questionnaire_question_pic`, `questionnaire_answer`, `questionnaire_choices`, `questionnaire_status`) VALUES
(1, 'SAVE', 'uploads/Dennis-Ritchie21.jpg', 'uploads/download7.png', 'uploads/Dennis-Ritchie22.jpg', 1),
(2, 'ANSWER', 'uploads/Dennis-Ritchie25.jpg', 'SAGOT', 'a1,a1,a1', 1),
(3, 'G4g4g4g4', 'uploads/Dennis-Ritchie23.jpg', 'uploads/Dennis-Ritchie24.jpg', 'uploads/Dennis-Ritchie26.jpg,uploads/Dennis-Ritchie27.jpg,uploads/download8.png,uploads/download9.png', 1),
(4, 'h3h3h3', 'uploads/Dennis-Ritchie28.jpg', 'z1z1z1', 'x1,x2,x3', 1),
(5, 'asdasfsdg', 'NULL', 'asdsgdfh', 'text,text', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `academic_sem`
--
ALTER TABLE `academic_sem`
  ADD PRIMARY KEY (`acad_id`);

--
-- Indexes for table `applicant`
--
ALTER TABLE `applicant`
  ADD PRIMARY KEY (`applicant_id`);

--
-- Indexes for table `application`
--
ALTER TABLE `application`
  ADD PRIMARY KEY (`application_id`);

--
-- Indexes for table `exam_result`
--
ALTER TABLE `exam_result`
  ADD PRIMARY KEY (`exam_result_id`);

--
-- Indexes for table `exam_template`
--
ALTER TABLE `exam_template`
  ADD PRIMARY KEY (`exam_id`);

--
-- Indexes for table `exam_template_questions`
--
ALTER TABLE `exam_template_questions`
  ADD PRIMARY KEY (`exam_template_questions_id`);

--
-- Indexes for table `questionnaire_bank`
--
ALTER TABLE `questionnaire_bank`
  ADD PRIMARY KEY (`questionnaire_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `academic_sem`
--
ALTER TABLE `academic_sem`
  MODIFY `acad_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `applicant`
--
ALTER TABLE `applicant`
  MODIFY `applicant_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `application`
--
ALTER TABLE `application`
  MODIFY `application_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `exam_result`
--
ALTER TABLE `exam_result`
  MODIFY `exam_result_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `exam_template`
--
ALTER TABLE `exam_template`
  MODIFY `exam_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `exam_template_questions`
--
ALTER TABLE `exam_template_questions`
  MODIFY `exam_template_questions_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `questionnaire_bank`
--
ALTER TABLE `questionnaire_bank`
  MODIFY `questionnaire_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
